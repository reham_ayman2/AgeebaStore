//
//  categoriModel.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 03/11/2021.
//

import Foundation
struct categoriModel : Codable , CodableInit {
    let value : String?
    let key : String?
    let data : [categorisData]
    let msg : String?
    let code : Int?

    enum CodingKeys: String, CodingKey {

        case value = "value"
        case key = "key"
        case data = "data"
        case msg = "msg"
        case code = "code"
    }}


struct categorisData : Codable {
    let slug : String
    let name : String

    enum CodingKeys: String, CodingKey {

        case slug = "slug"
        case name = "name"
    }}
