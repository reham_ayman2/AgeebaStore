//
//  timeModel.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 27/10/2021.
//

import Foundation

struct timeModel {
    let day : String
    let from : String
    let to : String
    
    func toDic()->[String:Any]{
         let dic = [
              "day" :  day ,
              "from" :from,
              "to" : to
           
              ] as [String : Any]
        
           return dic
       }
}


//struct AgebaUser : Codable , CodableInit {
//    let value : String?
//    let key : String?
//    let data : agebaUserData?
//    let msg : String?
//    let code : Int?
//
//    enum CodingKeys: String, CodingKey {
//
//        case value = "value"
//        case key = "key"
//        case data = "data"
//        case msg = "msg"
//        case code = "code"
//    }
//}
//struct agebaUserData : Codable {
//    let id : Int?
//    let name : String?
//    let phone : String?
//    let email : String?
//    let address : String?
//    let lat : Int?
//    let long : Int?
//    let delegate : String?
//    let avatar : String?
//    let device_id : String?
//    let active : String?
//    let code : String?
//    let time_zone : String?
//    let date : String?
//    let token : String?
//    let googlegooglekey : String?
//
//    enum CodingKeys: String, CodingKey {
//
//        case id = "id"
//        case name = "name"
//        case phone = "phone"
//        case email = "email"
//        case address = "address"
//        case lat = "lat"
//        case long = "long"
//        case delegate = "delegate"
//        case avatar = "avatar"
//        case device_id = "device_id"
//        case active = "active"
//        case code = "code"
//        case time_zone = "time_zone"
//        case date = "date"
//        case token = "token"
//        case googlekey = "googlekey"
//    }}
