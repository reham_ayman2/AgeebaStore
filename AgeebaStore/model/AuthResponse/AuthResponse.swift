//
//  AuthResponse.swift
//  SeaTrips
//
//  Created by Sara Mady on 11/5/20.
//  Copyright © 2020 Sara Ashraf. All rights reserved.
//

import Foundation

struct sendDefualtModel : Codable, CodableInit {
    let key: String
    let  code: Int
    let value : String
    let msg : String
    
    
  
}



struct AuthResponse: Codable ,CodableInit{
    let key: String
    let code: Int?
    let data: User
    let value : String
    let msg : String?
}

// MARK: - DataClass
struct User: Codable {
 
    
       let id : Int
       let name : String?
       let phone : String
       let email : String
       let address : String
       let lat : Double
       let long : Double
       let delegate : String
       let avatar : String
       let device_id : String
       let active : String
       let code : String?
       let time_zone : String
       let date : String
       let token : String
       let googlekey : String
       let approved : String
    
  
    
}


extension User {
    static var currentUser: User? {
        
        get {
            let userDefaults = UserDefaults.standard
            let decoder = JSONDecoder()
            guard let savedPerson = userDefaults.object(forKey: UserDefaultsKeys.user.rawValue) as? Data, let loadedPerson = try? decoder.decode(User.self, from: savedPerson) else { return nil }
            return loadedPerson
        }set {
            let userDefaults = UserDefaults.standard
            guard let value = newValue else {
                userDefaults.set(nil, forKey: UserDefaultsKeys.user.rawValue)
                return
            }
            
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(value) {
                userDefaults.set(encoded, forKey: UserDefaultsKeys.user.rawValue)
            }
        }
    }
}

enum UserDefaultsKeys: String {
    case user
}
