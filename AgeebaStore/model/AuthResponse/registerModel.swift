//
//  registerModel.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 22/11/2021.
//

import Foundation
struct registerModel : Codable , CodableInit{
    let value : String?
    let key : String?
    let data : RegisterData?
    let msg : String?
    let code : Int?

    enum CodingKeys: String, CodingKey {

        case value = "value"
        case key = "key"
        case data = "data"
        case msg = "msg"
        case code = "code"
    } }


struct RegisterData : Codable {
    let id : Int?
    let approved : String?
    let name : String?
    let phone : String?
    let email : String?
    let address : String?
    let lat : Double?
    let long : Double?
    let birthday : String?
    let delegate : String?
    let avatar : String?
    let device_id : String?
    let active : String?
    let code : String?
    let time_zone : String?
    let date : String?
    let token : String?
    let googlekey : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case approved = "approved"
        case name = "name"
        case phone = "phone"
        case email = "email"
        case address = "address"
        case lat = "lat"
        case long = "long"
        case birthday = "birthday"
        case delegate = "delegate"
        case avatar = "avatar"
        case device_id = "device_id"
        case active = "active"
        case code = "code"
        case time_zone = "time_zone"
        case date = "date"
        case token = "token"
        case googlekey = "googlekey"
    }}
