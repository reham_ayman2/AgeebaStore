//
//  ordersModel.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 05/11/2021.
//

import Foundation
struct OrdersModel : Codable , CodableInit {
    let value : String?
    let key : String?
    let data : [OrdersData]?
    let msg : String?
    let code : Int?

    enum CodingKeys: String, CodingKey {

        case value = "value"
        case key = "key"
        case data = "data"
        case msg = "msg"
        case code = "code"
    }

 
}


struct OrdersData : Codable {
    let id : Int?
    let user_id : Int?
    let user_name : String?
    let avatar : String?
    let details : [OrdersDetails]?
    let deliver_address : String?
    let date : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case user_name = "user_name"
        case avatar = "avatar"
        case details = "details"
        case deliver_address = "deliver_address"
        case date = "date"
    }
}



struct OrdersDetails : Codable {
    let key : String?
    let value : String?

    enum CodingKeys: String, CodingKey {

        case key = "key"
        case value = "value"
    }}
