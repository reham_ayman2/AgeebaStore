//
//  homeModel.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 05/11/2021.
//

import Foundation



struct HomeModel : Codable , CodableInit {
    let value : String
    let key : String
    let data : HomeData
    let msg : String
    let code : Int

    enum CodingKeys: String, CodingKey {

        case value = "value"
        case key = "key"
        case data = "data"
        case msg = "msg"
        case code = "code"
    }
}


struct HomeData : Codable {
    let num_open_orders : Int
    let num_inprogress_orders : Int
    let num_finished_orders : Int
    let num_products : Int

    enum CodingKeys: String, CodingKey {

        case num_open_orders = "num_open_orders"
        case num_inprogress_orders = "num_inprogress_orders"
        case num_finished_orders = "num_finished_orders"
        case num_products = "num_products"
    }
}


