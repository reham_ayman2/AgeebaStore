//
//  orderDetailsModel.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 12/11/2021.
//

import Foundation
struct orderDetailsModel : Codable , CodableInit {
    let value : String?
    let key : String?
    let data : OrderDetailsData?
    let msg : String?
    let code : Int?

    enum CodingKeys: String, CodingKey {

        case value = "value"
        case key = "key"
        case data = "data"
        case msg = "msg"
        case code = "code"
    }
}


struct OrderDetailsData : Codable {
    let id : Int?
    let username : String?
    let avatar : String?
    let rating : Double?
    let needs_delivery : String?
    let deliver_time : String?
    let receive_lat : Double?
    let receive_long : Double?
    let receive_address : String?
    let deliver_lat : Double?
    let deliver_long : Double?
    let deliver_address : String?
    let price : String?
    let min_expected_price : Int?
    let max_expected_price : Int?
    let details : [ODetails]?
    let status : String?
    let store_status : String?
    let have_invoice : String?
    let paymentMethod : String?
    let paymentMethod_ : String?
    let payment_status : Int?
    let created_at : String?
    let products : [Products]?
    let order_step : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case username = "username"
        case avatar = "avatar"
        case rating = "rating"
        case needs_delivery = "needs_delivery"
        case deliver_time = "deliver_time"
        case receive_lat = "receive_lat"
        case receive_long = "receive_long"
        case receive_address = "receive_address"
        case deliver_lat = "deliver_lat"
        case deliver_long = "deliver_long"
        case deliver_address = "deliver_address"
        case price = "price"
        case min_expected_price = "min_expected_price"
        case max_expected_price = "max_expected_price"
        case details = "details"
        case status = "status"
        case store_status = "store_status"
        case have_invoice = "have_invoice"
        case paymentMethod = "paymentMethod"
        case paymentMethod_ = "paymentMethod_"
        case payment_status = "payment_status"
        case created_at = "created_at"
        case products = "products"
        case order_step = "order_step"
    }
}
struct ODetails : Codable {
    let key : String?
    let value : String?

    enum CodingKeys: String, CodingKey {

        case key = "key"
        case value = "value"
    }}




struct Products : Codable {
    let name : String?
    let image : String?
    let size : String?
    let selectedDetails : String?
    let count : String?
    let id : Int?
    let price : String?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case image = "image"
        case size = "size"
        case selectedDetails = "selectedDetails"
        case count = "count"
        case id = "id"
        case price = "price"
    }}
