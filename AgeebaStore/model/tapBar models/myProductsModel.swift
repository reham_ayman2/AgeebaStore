//
//  myProductsModel.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 11/11/2021.
//

import Foundation


struct productModel : Codable  , CodableInit{
    let value : String
    let key : String
    let data : [ProductData]?
    let msg : String
    let code : Int

    enum CodingKeys: String, CodingKey {

        case value = "value"
        case key = "key"
        case data = "data"
        case msg = "msg"
        case code = "code"
    }}


struct ProductData : Codable {
    let id : Int?
    let name : String?
    let price : String?
    let description : String?
    let image : String?
    let status : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case price = "price"
        case description = "description"
        case image = "image"
        case status = "status"
    }}
