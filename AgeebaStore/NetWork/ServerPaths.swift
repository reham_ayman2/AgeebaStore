//
//  ServerPaths.swift
//  SeaTrips
//
//  Created by Sara Mady on 10/27/20.
//  Copyright © 2020 Sara Ashraf. All rights reserved.
//

import Foundation
import SwiftUI

enum ServerPaths: String {
    
    //MARK:- store  APIS:-
    
   case categories
   case storeRegister
   case countries
    case storeAccountActivation
    case storeForgetPassword
    case storeResetPassword
    case storeLogin
    case storeHome
    case storeOpenOrders
    case storeActiveOrders
    case storeFinishedOrders
    case storeProducts
    case storeMyProducts
    case storeOrderDetails
    case storeCancelOrder
    case storeAcceptOrder 
    
    
    

}
