//
//  AppDelegate.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 10/10/2021.
//

import UIKit
import IQKeyboardManagerSwift
import CoreLocation
import GoogleMaps
import GooglePlaces
import MapKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static var FCMTOKEN = "1234567"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarTintColor = UIColor.BasicColor
        self.setUpSegment()
        GMSServices.provideAPIKey("AIzaSyBrEyBXL7xP6XwzTnybqmYciGjjxsYF5xI")
        GMSPlacesClient.provideAPIKey("AIzaSyBrEyBXL7xP6XwzTnybqmYciGjjxsYF5xIs")

       
        
        
            
            
        return true
    }

    
    
    
    
    
 
    
    // MARK: - home storyboard
    
    
    func GoToHomeStoryBoard(){
        
//        let vc = Storyboard.Main.viewController(StartNavBar.self)
//        self.window?.rootViewController = vc
//        window?.makeKeyAndVisible()
       
        let sb:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc2 = sb.instantiateViewController(withIdentifier: "StartNavBar")

        self.window?.rootViewController = vc2
      
    }
    
    func gotoAuth () {
        let vc = UIStoryboard.instantiateInitialViewController ( .Main)
        self.window?.rootViewController = vc
        window?.makeKeyAndVisible()
        
    }
    
    
    
    
    func setUpSegment() {
        
        
        if let font = UIFont(name: "Cairo-SemiBold" , size: 15 )
        {UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
            
        }
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor:UIColor.white], for: .selected)
        
        
    }



}

