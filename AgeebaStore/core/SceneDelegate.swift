//
//  SceneDelegate.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 10/10/2021.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {



        if let windowScene = scene as? UIWindowScene {
           let window = UIWindow(windowScene: windowScene)

        if User.currentUser != nil , KeyChain.userToken != nil  {
            if KeyChain.tokenExist , User.currentUser?.active == "active" {
               

                let sb:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc2 = sb.instantiateViewController(withIdentifier: "StartNavBar")

                self.window?.rootViewController = vc2
                window.makeKeyAndVisible()




            }else{


                let vc = UIStoryboard.instantiateInitialViewController ( .Main)
                self.window?.rootViewController = vc
                window.makeKeyAndVisible()



            }
        }else{




            let vc = UIStoryboard.instantiateInitialViewController ( .Main)
            self.window?.rootViewController = vc
            window.makeKeyAndVisible()

        }
        }





    }

    

    
    
    

}

