//
//  NextRegisterVC+delegate.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 04/11/2021.
//

import Foundation
import SKActivityIndicatorView


extension NextRegsterVC : NextRegisterView {
    func gotoVerfication() {
        let vc = Storyboard.Main.viewController(ActivationCodeVC.self)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showIndicator() {
        SKActivityIndicator.show()
    }
    
    func hideIndicator() {
        SKActivityIndicator.dismiss()
    }
    
    func showError(error: String) {
        showErrorAlert(title: "", message: error)
    }
    func showSuccessmsg(msg: String) {
        showSuccessAlert(title: "", message: msg)
    }
    
    
    
    
}
