//
//  NextRegsterVC.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 04/11/2021.
//

import UIKit
import SwiftyJSON

class NextRegsterVC: UIViewController {
    
    
    //MARK: - VARS & CONST
    
    var presenter : NextRegisterVcPresnter!
    var iconClick = true
    
    // paramters come from 1st register vc
    
    var arabicName = ""
    var englishName = ""
    var categoriSlug = ""
    var commericalNum = ""
    var website = ""
    var lat = 0.0
    var long = 0.0
    var CountryIso = "" 
    var times = ""
    var uploadImge = [UploadData]()
    var ArrayofTimes = [timeModel]()
    var utils = AppUtils.getObject()
     
      
      //MARK: - OUTLETS
    
    @IBOutlet weak var bankName: UITextField!
    @IBOutlet weak var name: UITextField!
    
    @IBOutlet weak var mobileNumber: UITextField!
    
    @IBOutlet weak var nationalId: UITextField!
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var address: UITextField!
    
    @IBOutlet weak var ibanNumber: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
      //MARK: - APP CYCLE
      
    

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = NextRegisterVcPresnter(view: self)
        print("🤚🏻params : ")
        print(arabicName)
        print(englishName)
        print(uploadImge)
        print(website)
        print(lat)
        print(long)
        print(commericalNum)
        print(ArrayofTimes)
   
     
        

    }
    

    //MARK: - ACTIONS

    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func confirm(_ sender: UIButton) {
        
        if self.name.text == "" || self.mobileNumber.text == "" || self.nationalId.text == "" || self.email.text == "" || self.address.text == "" || self.ibanNumber.text == "" || self.password.text == ""   {
            
            showWarningAlert(title: "", message: "Please Complete Your Data First!")
            
        } else  if (utils.isValidEmail(testStr: self.email.text ?? ""  ) == false) {
            showWarningAlert(title: "", message: "Wrong Email !")

        }
            
        else {
             var additionJson = [[String:Any]]()
             for item in ArrayofTimes {
                 additionJson.append(item.toDic())
             }
             let activitesData = try? JSONSerialization.data(withJSONObject: additionJson, options: .prettyPrinted)
             self.times = String(data: activitesData!, encoding: String.Encoding.utf8)!
             
            print("⏰⏰⏰⏰⏰⏰⏰⏰⏰⏰⏰⏰")
            print(self.times)
            
            
            
            presenter.signUpNewUser(phone: self.mobileNumber.text ?? "" ,
                                    country_iso: "SA",
                                    device_type: "ios",
                                    device_id: AppDelegate.FCMTOKEN,
                                    name: self.name.text ?? "" ,
                                    email: self.email.text ?? "" ,
                                    address: self.address.text ?? "" ,
                                    lat: self.lat,
                                    long: self.long,
                                    password: self.password.text ?? "" ,
                                    name_ar: self.arabicName,
                                    name_en: self.englishName,
                                    category: self.categoriSlug,
                                    website: self.website,
                                    identity_card: self.nationalId.text ?? ""  ,
                                    commercial: self.commericalNum,
                                    bank_iban_number:  self.ibanNumber.text ?? "" ,
                                    bankName: self.bankName.text ?? ""  ,
                                    working_times: self.times ,
                                    Images: self.uploadImge)
        }
        
        
        
    }
    
    @IBAction func viewpassword(_ sender: UIButton) {
        if(iconClick == true) {
            password.isSecureTextEntry = false
        } else {
           password.isSecureTextEntry = true
        }

        iconClick = !iconClick
    }
}
