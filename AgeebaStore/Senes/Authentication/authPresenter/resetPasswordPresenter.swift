//
//  resetPasswordPresenter.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 06/11/2021.
//

import Foundation




protocol resetPasswordVcView: class {
    func showIndicator()
    func hideIndicator()
    func showError(error:String)
    func gotoLoginScreen ()
 

  
}



//MARK: - PRESENTER CLASS
class resetPasswordVcPresnter {
    
    private weak var view: resetPasswordVcView?
    
    
    
    
    init(view:resetPasswordVcView) {
        self.view = view
    }
    
    
    
    
    func ResetPasswordPssword(code:String , password : String ){
        view?.showIndicator()
        AgebaInteractor.storeResetPassword(code: code, password: password).send(AuthResponse.self){
            [weak self] (response) in

            guard let self = self else { return }
            self.view?.hideIndicator()
            switch response {
            case .unAuthorized(_):
                print("unAuthorized")
            case .failure(let error):
                print("failure\(String(describing: error))")
            case .success(let value):
                
                if value.key == "success" {
                    self.view?.gotoLoginScreen()
                } else {
                    print("error")
                }
                
                
                
                
                
            case .errorResponse(let error):
                guard let errorMessage = error as? APIError else { return  showNoInterNetAlert()}
                self.view?.showError(error: errorMessage.localizedDescription.localized)
            }
        }

    }
    
    
    
    
    func cheackValidData(code:String , password : String , confrimPassword : String) {

        
    
        
        
        
        if(code == "" || password == "" || confrimPassword == ""){

            completData(title: "", message: "please complete data ")
            
            
            
       
        } else if password != confrimPassword  {
            showWarningAlert(title: "", message: "password and confirm password must be match!")

           
        } else {
            
            
            self.ResetPasswordPssword(code: code, password: password)
            
        }

    }

    
    
   
    
    

    
    
}
