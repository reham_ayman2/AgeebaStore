//
//  forgetPasswordPresenter.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 11/10/2021.
//

import Foundation
//MARK: - PROTOCOLS
protocol ForgetPasswordView: class {
    func showIndicator()
    func hideIndicator()
    func showError(error:String)
    
    func goToActtvtionScreen()
}




//MARK: - PRESENTER CLASS
class forgetPasswordVcPresnter {
    
    private weak var view: ForgetPasswordView?
    var utils = AppUtils()
    
    
    init(view:ForgetPasswordView) {
        self.view = view
    }
    
    
    func viewDidLoad(){
        
     
    }
    
    
    
    
    func ForgetPssword(phone:String){
        view?.showIndicator()
        AgebaInteractor.storeForgetPassword(mobile: phone).send(AuthResponse.self){
            [weak self] (response) in

            guard let self = self else { return }
            self.view?.hideIndicator()
            switch response {
            case .unAuthorized(_):
                print("unAuthorized")
            case .failure(let error):
                print("failure\(String(describing: error))")
            case .success(let value):
                
                if value.key == "success" {
                
                    self.view?.goToActtvtionScreen()
                }
                
            case .errorResponse(let error):
                guard let errorMessage = error as? APIError else { return  showNoInterNetAlert()}
                self.view?.showError(error: errorMessage.localizedDescription.localized)
            }
        }

    }
    
    
    
    
    func cheackValidData(mobile:String) {

        
    
        
        
        
        if(mobile != ""){

            self.ForgetPssword(phone: mobile)
       
        } else  {

            showWarningAlert(title: "", message: "Please enter the Mobile number First")
        }

    }

    
    


  
}
