//
//  loginPresenter.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 11/10/2021.
//

import Foundation

//MARK: - PROTOCOLS
protocol LoginView: class {
    func showIndicator()
    func hideIndicator()
    func showError(error:String)
    
    
    func goToRegister()
    func goTOhome()
    func gotoForgetPassword()
    func LoginToApplication ()
    func goToActtvtionScreen()
    func goToAlertACCeptionVC()
}




//MARK: - PRESENTER CLASS
class LoginVcPresnter {
    
    private weak var view: LoginView?
    var utils = AppUtils()
    
    
    init(view:LoginView) {
        self.view = view
    }
    
    
    func viewDidLoad(){
        
     
    }
    
    
    
    
    func LoginUser(phone:String,password:String){
        view?.showIndicator()
        AgebaInteractor.storeLogin(phone: phone, password: password, country_iso: "SA", device_id: AppDelegate.FCMTOKEN, device_type: "ios").send(AuthResponse.self){
            [weak self] (response) in

            guard let self = self else { return }
            self.view?.hideIndicator()
            switch response {
            case .unAuthorized(_):
                print("unAuthorized")
            case .failure(let error):
                print("failure\(String(describing: error))")
            case .success(let value):
                if(value.data.active == "pending"){
                    KeyChain.userToken = value.data.token
                    self.view?.goToActtvtionScreen()
                }else if value.data.approved == "false"{
                    
                    self.view?.goToAlertACCeptionVC()
                }
                else {
                    print("✅")
                    KeyChain.userToken = value.data.token
                    User.currentUser = value.data
                    self.view?.LoginToApplication()
                    defaults.set( value.data.googlekey, forKey: GOOGLE_KEY)
                    defaults.set(value.data.email , forKey: User_Email)
                    defaults.set("no", forKey: is_Vistor)
                    defaults.set(value.data.name , forKey: User_Name)
                 
                    defaults.set(value.data.address, forKey: User_Address)
                    defaults.set(value.data.token, forKey: Token_ID)
                    defaults.set(value.data.phone , forKey: Mobile_Number)
                    defaults.set(String(value.data.id), forKey: User_ID)
                    defaults.set(value.data.avatar, forKey: User_Avatar)
                    
               
                }
            case .errorResponse(let error):
                guard let errorMessage = error as? APIError else { return  showNoInterNetAlert()}
                self.view?.showError(error: errorMessage.localizedDescription.localized)
            }
        }

    }
    
    
    
    
    func cheackValidData(mobile:String,password:String) {

        
    
        
        
        
        if(mobile != "" && password != ""){

            self.LoginUser(phone: mobile, password: password)
       
        } else  {

            completData(title: "", message: "Please enter the Mobile number and password".localized)
        }

    }

    
    func gortoForgetPassword () {
        self.view?.gotoForgetPassword()
    }

 
    func GotoRegisterScreen () {
        self.view?.goToRegister()
    }
    func gotoHomeScreen () {
        self.view?.goTOhome()
    }
  
}
