//
//  activationPresnter.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 06/11/2021.
//

import Foundation


protocol ActivationVcView : class {
    func showIndicator()
    func hideIndicator()
    func showError(error:String)
    func gotoResetPassswordSCreen ()
    func showSuccess( msg : String)
    func gotoSuccessScreen ()
   
}



// MARK: - PRESENTER CLASS
class activationVcPresnter {
    
    private weak var view: ActivationVcView?
    
    
    init(view:ActivationVcView) {
        self.view = view
    }
    
    
  
    func gotoResetScreen () {
        self.view?.gotoResetPassswordSCreen()
    }
    
    
    
    func VerfyPhone(v_code: String ){
        view?.showIndicator()
       AgebaInteractor.storeAccountActivation(code: v_code).send(AuthResponse.self){
            [weak self] (response) in

            guard let self = self else { return }
            self.view?.hideIndicator()
            switch response {
            case .unAuthorized(_):
                print("unAuthorized")
            case .failure(let error):
                print("failure\(String(describing: error))")
            case .success(let value):
                KeyChain.userToken = value.data.token
                    User.currentUser = value.data
                    self.view?.showSuccess(msg: "Your account has been successfully Registered".localized)
                
                self.view?.gotoSuccessScreen()
            case .errorResponse(let error):
                guard let errorMessage = error as? APIError else { return  showNoInterNetAlert()}
                self.view?.showError(error: errorMessage.localizedDescription.localized)
            }
        }
    }
    

    
    
}
