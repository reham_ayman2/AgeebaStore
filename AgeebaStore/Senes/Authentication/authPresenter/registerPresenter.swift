//
//  registerPresenter.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 11/10/2021.
//

import Foundation
//MARK: - PROTOCOLS
protocol RegisterView: class {
    
    
    func showIndicator()
    func hideIndicator()
    func showError(error:String)
    func goTOhome()
    func makeImageRound ()
    func goSelectLocation()
    func HandelTabel ()
    func showImagePicker()
    func showCoverPicker()
    func goToSelectSection()
    func goToNextStep()
    func showWarningALERT(aleert: String)
    func showCommeriaclImagePicker ()
    
}




//MARK: - PRESENTER CLASS
class RegisterVcPresnter {
    
    private weak var view: RegisterView?
    var utils = AppUtils.getObject()
    
    
    init(view:RegisterView) {
        self.view = view
    }
    
    
    func viewDidLoad(){
        self.view?.makeImageRound()
        self.view?.HandelTabel()
     
    }
    
    
    
    func goSelectLocationVC () {
        self.view?.goSelectLocation()
    }
   
    func showCommericalPicker() {
        self.view?.showCommeriaclImagePicker()
    }
 
    func gotoHomeScreen () {
        self.view?.goTOhome()
    }
    
    
    func showImgaePicker () {
        self.view?.showImagePicker()
    
    }
    
    
    
    func showCoverPicker()  {
        self.view?.showCoverPicker()
        
    }
    
    
    func goSelectSection () {
        self.view?.goToSelectSection()
    }
    func goToNextPage () {
        self.view?.goToNextStep()
    }
    
   
    
    
    func cheackData(nameAR:String,nameEN:String,section:String,CommercialNum:String,website:String ,location : String  ){
        
        if(nameAR == "" || nameEN == "" || section == "" ||  CommercialNum == "" || website == ""  || location == ""  ) {
            completData(title: "", message: "Please enter all required information".localized)
        }else {
            self.view?.goToNextStep()

        }
    }
    
  
    
    
    
}
