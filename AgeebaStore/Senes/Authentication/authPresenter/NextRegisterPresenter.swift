//
//  NextRegisterPresenter.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 04/11/2021.
//

import Foundation

protocol NextRegisterView: class {
    func showIndicator()
    func hideIndicator()
    func showError(error:String)
    func gotoVerfication()
    func showSuccessmsg(msg : String )

  
}



//MARK: - PRESENTER CLASS
class NextRegisterVcPresnter {
    
    private weak var view: NextRegisterView?
    
    
    
    
    init(view:NextRegisterView) {
        self.view = view
    }
    
    
    func viewDidLoad(){
        
     
        
        
     
    }
    
    
    func signUpNewUser (phone: String, country_iso: String, device_type: String , device_id: String, name: String, email: String, address: String, lat: Double, long: Double, password: String, name_ar: String, name_en: String, category: String, website: String, identity_card: String, commercial: String, bank_iban_number: String, bankName: String, working_times:  String ,  Images : [UploadData]) {
        
        self.view?.showIndicator()
        
        AgebaInteractor.storeRegister(phone: phone, country_iso: country_iso, device_type: "ios", device_id: AppDelegate.FCMTOKEN, name: name, email: email, address: address, lat: lat, long: long, password: password, name_ar: name_ar, name_en: name_en, category: category, website: website, identity_card: identity_card, commercial: commercial, bank_iban_number: bank_iban_number, bankName: bankName, working_times: working_times).send(registerModel.self, data: Images ,  completion: {
            [weak self] (response) in
            
            guard let self = self else { return }
            self.view?.hideIndicator()
            switch response {
            case .unAuthorized(_):
                print("unAuthorized🤥")
            case .failure(let error):
                
                print("failure❌\(String(describing: error))")
            case .success(let value):
                KeyChain.userToken = value.data?.token
                print("success to register🟢✅ ")
                
                self.view?.showSuccessmsg(msg: "send verification code successfully .. ".localized)
                self.view?.gotoVerfication()
            case .errorResponse(let error):
                self.view?.hideIndicator()
                guard let errorMessage = error as? APIError else { return  showNoInterNetAlert()}
                self.view?.showError(error: errorMessage.localizedDescription.localized)
            }
        })
        
        
        
        
        
    }
    
    
    

    
    
}


