//
//  selectSectionVCPresenter.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 03/11/2021.
//

import Foundation
//MARK: - PROTOCOLS
protocol selectSectionView: class {
    func showIndicator()
    func hideIndicator()
    func showError(error:String)
    func setData(data : [categorisData])
    func pickerSetup()
  
}




//MARK: - PRESENTER CLASS
class selectSectionVcPresnter {
    
    private weak var view: selectSectionView?
    var categiresArray = [categorisData]()
    
    
    
    init(view:selectSectionView) {
        self.view = view
    }
    
    
    func viewDidLoad(){
        
        self.view?.pickerSetup()
        getCategoriesData()
        
        
     
    }
    
    
    func getCategoriesData(){
        view?.showIndicator()
        AgebaInteractor.categories.send(categoriModel.self) {
            [weak self] (response) in
            guard let self = self else { return }
            self.view?.hideIndicator()
            switch response {
            case .unAuthorized(_):
                
                print("unAuthorized")
                
            case .failure(let error):
                
                showNoInterNetAlert()
                print("failure\(String(describing: error))")
                
            case .success(let value):
                print("hii")
                
                
                self.categiresArray = value.data
                self.view?.setData(data: value.data)
                
              
                
            case .errorResponse(let error):
                guard let errorMessage = error as? APIError else { return  showNoInterNetAlert()}
                self.view?.showError(error: errorMessage.localizedDescription.localized)
            }
        }

    }
    
    
    
    

    
    
}
