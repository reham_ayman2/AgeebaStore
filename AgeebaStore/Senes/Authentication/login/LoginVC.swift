//
//  ViewController.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 10/10/2021.
//

import UIKit

class LoginVC: UIViewController {
    
    //MARK: - VARS & CONST
   
    var presenter:LoginVcPresnter!
    var iconClick = true
    
    
    
    //MARK: - OUTLETS
    
    @IBOutlet weak var mobileText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    
    
    
    //MARK: - APP CYCLE
    


    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = LoginVcPresnter(view: self)
      
    }

    
    //MARK: - ACTIONS
 
    @IBAction func register(_ sender: UIButton) {
        self.presenter.GotoRegisterScreen()
        
    }
    @IBAction func loginPressed(_ sender: UIButton) {
      
        presenter.cheackValidData(mobile: mobileText.text ?? "" , password: passwordText.text ?? "")
        
      
    }
    @IBAction func forgetPassword(_ sender: UIButton) {
        self.presenter.gortoForgetPassword()
    }
    @IBAction func iconAction(sender: AnyObject) {
            if(iconClick == true) {
                passwordText.isSecureTextEntry = false
            } else {
                passwordText.isSecureTextEntry = true
            }

            iconClick = !iconClick
        }
    
}

  
