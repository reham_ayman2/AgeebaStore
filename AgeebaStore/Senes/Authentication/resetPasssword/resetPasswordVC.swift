//
//  resetPasswordVC.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 06/11/2021.
//

import UIKit

class resetPasswordVC: UIViewController {

    
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmNewPasword: UITextField!
    var code = ""
    var iconClick = true
    var confirmClick = true
    var presenter : resetPasswordVcPresnter!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = resetPasswordVcPresnter(view: self)

    }
    

    @IBAction func showPass(_ sender: UIButton) {
        if(iconClick == true) {
            newPassword.isSecureTextEntry = false
        } else {
            newPassword.isSecureTextEntry = true
        }

        iconClick = !iconClick
    }
    
    @IBAction func confrim(_ sender: UIButton) {
        
        self.presenter.cheackValidData(code: self.code, password: self.newPassword.text ?? "" , confrimPassword: self.confirmNewPasword.text ?? "" )
        
        
        
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showConfirmPass(_ sender: UIButton) {
        if(confirmClick == true) {
            confirmNewPasword.isSecureTextEntry = false
        } else {
            confirmNewPasword.isSecureTextEntry = true
        }

        confirmClick = !confirmClick
        
    }
}
