//
//  resetPassword+delegte.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 06/11/2021.
//

import Foundation
import SKActivityIndicatorView


extension resetPasswordVC : resetPasswordVcView {
    func showIndicator() {
        SKActivityIndicator.show()
    }
    
    func hideIndicator() {
        SKActivityIndicator.dismiss()
    }
    
    func showError(error: String) {
        showErrorAlert(title: "", message: error)
    }
    
    func gotoLoginScreen() {
        showSuccessAlert(title: "", message: "You have changed Password successfully".localized)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            guard let window = UIApplication.shared.keyWindow else { return }
            let vc = Storyboard.Main.initialViewController()
            window.rootViewController = vc

        }
    }
    
    
    
}
