//
//  loginVC+PresenterDelegate.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 11/10/2021.
//

import Foundation
import SKActivityIndicatorView


extension LoginVC : LoginView {
    
    
    func LoginToApplication() {
        showSuccessAlert(title: "", message: "You have signed in successfully".localized)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            guard let window = UIApplication.shared.keyWindow else { return }
            let vc = Storyboard.Main.viewController(StartNavBar.self)
            window.rootViewController = vc

        }
      
        
    }
    func goToAlertACCeptionVC() {
        let vc = Storyboard.Main.viewController(acceptScreen.self)
        self.present(vc, animated: true, completion: nil)
    }
    func goToActtvtionScreen() {
        print("go to activation vc ")
       
    }
    
    func showIndicator() {
        SKActivityIndicator.show()
    }
    
    func hideIndicator() {
        SKActivityIndicator.dismiss()
        
    }
    
    func showError(error: String) {
        showErrorAlert(title: "", message: error)
        
    }
    
  
    
    
    
    func goToRegister() {
        
        let vc = Storyboard.Main.viewController(RegisterVC.self)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goTOhome() {
        
    }
    
    func gotoForgetPassword() {
        let vc = Storyboard.Main.viewController(forgetPasswordVC.self)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
}
