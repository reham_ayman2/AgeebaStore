//
//  forgetPasswordVC.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 11/10/2021.
//

import UIKit

class forgetPasswordVC: UIViewController {

    
    
    //MARK: - VARS & CONST
      
    var presenter : forgetPasswordVcPresnter!
       
       //MARK: - OUTLETS
       
       
    @IBOutlet weak var mobileText: UITextField!
    
    
    
    
       //MARK: - APP CYCLE
       
    
    override func viewDidLoad() {
        super.viewDidLoad()

      presenter = forgetPasswordVcPresnter(view: self)
        
    }
    
    //MARK: - ACTIONS
  
    @IBAction func confirm(_ sender: UIButton) {
        self.presenter.cheackValidData(mobile:  self.mobileText.text ?? "" )
        
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
