//
//  ForgetPasswordVC+PresenterDelegate.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 11/10/2021.
//

import Foundation
import SKActivityIndicatorView

extension forgetPasswordVC : ForgetPasswordView {
    func showIndicator() {
        SKActivityIndicator.show()
    }
    
    func hideIndicator() {
        SKActivityIndicator.dismiss()
    }
    
    func showError(error: String) {
        showErrorAlert(title: "", message: error)
    }
    
    func goToActtvtionScreen() {
        showSuccessAlert(title: "", message: "The code has been sent successfully".localized)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        let vc = Storyboard.Main.viewController(ActivationCodeVC.self)
            vc.ComeFromForgetPasswordVC = true
        self.navigationController?.pushViewController(vc, animated: true)
        }
    
        }
    
    
}
