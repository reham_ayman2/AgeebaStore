//
//  AuthntcationInteractor.swift
//  Ra3ia
//
//  Created by Sara Mady on 04/04/2021.
//

import Foundation
import Alamofire


enum AgebaInteractor: URLRequestBuilder {
    
    
    case categories
    case storeLogin(phone : String , password : String , country_iso : String , device_id : String , device_type : String )
    
    
    
    case storeRegister( phone : String ,
                        country_iso : String ,
                        device_type : String ,
                        device_id : String ,
                        name : String ,
                        email : String ,
                        address : String ,
                        lat : Double ,
                        long : Double ,
                        password : String ,
                        name_ar : String ,
                        name_en : String ,
                        category : String ,
                        website : String ,
                        identity_card : String ,
                        commercial : String ,
                        bank_iban_number : String ,
                        bankName : String ,
                        working_times :  String )
    

    
    case storeHome
    case storeFinishedOrders
    case storeActiveOrders
    case storeForgetPassword( mobile : String )
    case storeResetPassword( code : String , password : String)
    case storeMyProducts
    case storeOpenOrders
    case storeOrderDetails ( Orderid : Int , lat : Double , long : Double)
    case storeAccountActivation ( code : String )
    case storeCancelOrder ( order_id : Int , long : Double )
    case storeAcceptOrder( order_id : Int , long : Double )
    

    
    // MARK: - Paths
    internal var path: ServerPaths {
        
        switch self {
        case .categories :
            
            return .categories
        case .storeAccountActivation:
            return .storeAccountActivation
        case .storeLogin:
            return .storeLogin
        case .storeHome:
            return .storeHome
        case .storeFinishedOrders:
            return .storeFinishedOrders
        case .storeActiveOrders:
            return .storeActiveOrders
        case .storeForgetPassword:
            return .storeForgetPassword
        case .storeResetPassword:
            return .storeResetPassword
        case .storeMyProducts:
            return .storeMyProducts
        case .storeOpenOrders:
            return .storeOpenOrders
        case .storeOrderDetails:
            return .storeOrderDetails
        case .storeRegister:
            return .storeRegister
        case .storeCancelOrder:
            return .storeCancelOrder
        case .storeAcceptOrder:
            return .storeAcceptOrder
      
        }}
    
    // MARK: - Parameters
    internal var parameters: Parameters? {
        var params = Parameters()
        
        switch self {
        
        
            
        case .storeLogin(phone: let phone, password: let password, country_iso: let country_iso, device_id: let device_id , device_type: let device_type):
            params["phone"] = phone
            params["password"] = password
            params["country_iso"] = country_iso
            params["device_id"] = device_id
            params["device_type"] = device_type
            
        case .storeForgetPassword(mobile: let mobile):
            params["phone"] = mobile
        case .storeResetPassword(code: let code , password: let password):
        params["code"] = code
        params["password"] = password
        case .storeOrderDetails(Orderid: let orderID , lat: let lat , long: let long ):
            params["order_id"] = orderID
            params["lat"] = lat
            params["long"] = long
            
            
        case .storeRegister(phone: let phone, country_iso: let country_iso, device_type: let device_type, device_id: let device_id, name: let name, email: let email, address: let address, lat: let lat, long: let long, password: let password, name_ar: let name_ar, name_en: let name_en, category: let category, website: let website, identity_card: let identity_card, commercial: let commercial, bank_iban_number: let bank_iban_number, bankName: let bankName, working_times: let working_times):
            params["phone"] = phone
            params["country_iso"] = country_iso
            params["device_type"] = device_type
            params["device_id"] = device_id
            params["name"] = name
            params["email"] = email
            params["address"] = address
            params["lat"] = lat
            params["long"] = long
            params["password"] = password
            params["name_ar"] = name_ar
            params["name_en"] = name_en
            params["category"] = category
            params["website"] = website
            params["identity_card"] = identity_card
            params["commercial"] = commercial
            params["bank_iban_number"] = bank_iban_number
            params["bankName"] = bankName
            params["working_times"] = working_times
                  
            
            case .storeAccountActivation(code : let code):
            params["code"] = code
            
        case .storeCancelOrder(order_id: let order_id , long: let long ):
            params["order_id"] = order_id
            params["long"] = long
            
        case .storeAcceptOrder(order_id: let order_id , long: let long ):
            params["order_id"] = order_id
            params["long"] = long
            
       
      
        
        case .categories , .storeHome , .storeFinishedOrders , .storeActiveOrders , .storeMyProducts , .storeOpenOrders  :
      
            break
           
       
       
        }
        print("☠️\(params)")
        return params
        
    }
    
    
    internal var insideUrlParam: [String]?{
        switch self {
     
        default:
            return nil
        }
    }
    
    internal var headers: HTTPHeaders{
        var header = HTTPHeaders()
        switch self {
    
        
        default:
            if let token = KeyChain.userToken {
                header["Authorization"] = "Bearer \(token)"
            }
            header["lang"] = AppLanguage.getLang()
            
        }
        
        return header
    }
    
    var fcmTokenDevice: String {
        return  "token"
    }
    
    // MARK: - Method
    
    internal var method: HTTPMethod {
        switch self {
        case .categories , .storeHome , .storeFinishedOrders , .storeActiveOrders , .storeMyProducts , .storeOpenOrders  :
            return .get
        default:
            return .post
        }
    }
    
    // MARK: - Encoding
    
    internal var encoding: ParameterEncoding {
        switch self {
        case .categories , .storeHome , .storeFinishedOrders , .storeActiveOrders , .storeMyProducts , .storeOpenOrders :
            return URLEncoding.default

        default:
            return JSONEncoding.default
        }
    }
}
