//
//  activationVC+PresnterDelegate.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 06/11/2021.
//

import Foundation
import SKActivityIndicatorView


extension ActivationCodeVC : ActivationVcView {
    func gotoSuccessScreen() {
        let vc = Storyboard.Main.viewController(acceptScreen.self)
      //  vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func showSuccess(msg: String) {
        showSuccessAlert(title: "" , message: msg)
    }
    
    func showIndicator() {
        SKActivityIndicator.show()
    }
    
    func hideIndicator() {
        SKActivityIndicator.dismiss()
    }
    
    func showError(error: String) {
        showErrorAlert(title: "", message: error)
    }
    
    func gotoResetPassswordSCreen() {
        
        if code.text == "" {
            
           showWarningAlert(title: "", message: "enter the code first !")
            
            
        } else {
        
        
        
        
        let vc = Storyboard.Main.viewController(resetPasswordVC.self)
        vc.code = self.code.text ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
    
    
}
