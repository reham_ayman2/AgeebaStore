//
//  ActivationCodeVC.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 06/11/2021.
//

import UIKit

class ActivationCodeVC: UIViewController {
    var ComeFromForgetPasswordVC = false
    var presenter : activationVcPresnter!

    
    @IBOutlet weak var code: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = activationVcPresnter(view: self)

    }
    
    @IBAction func confirm(_ sender: UIButton) {
        if ComeFromForgetPasswordVC {
            self.presenter.gotoResetScreen()
            
        } else {
            presenter.VerfyPhone(v_code: self.code.text ?? "")
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
