//
//  registerCell.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 11/10/2021.
//

import UIKit

class registerCell: UITableViewCell  , selecttime , selectDay , selecttime2{
   
    func selectedtime2(date: String, dateFormmater: String) {
        self.fromText.text = date
        self.selectFromFOrmate = dateFormmater
        
        
    }
    func selectedtime(date: String , dateFormate:  String) {
        self.toText.text = date
        self.selectToFormate = dateFormate
    }
    
    func selectedDay(date: String) {
        self.dayText.text = date
    }
    
   
    var toTimeSelect : (() -> ())?
    var fromTimeSelect : (() -> ())?
    var dateSelect : (() -> ())?
    var ADDSelect : (() -> ())?
    var selectFromFOrmate = ""
    var selectToFormate = ""
    
    
    
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var dayText: UITextField!
    @IBOutlet weak var toText: UITextField!
    @IBOutlet weak var fromText: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func addButton(_ sender: UIButton) {
        self.ADDSelect?()
    }
    
    @IBAction func toButton(_ sender: UIButton) {
        self.toTimeSelect?()
    }
    @IBAction func fromButton(_ sender: UIButton) {
        self.fromTimeSelect?()
    }
    
    @IBAction func dayButton(_ sender: UIButton) {
        self.dateSelect?()
    }
}
