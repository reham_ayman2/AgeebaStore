//
//  RegisterVC+tableview.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 11/10/2021.
//

import UIKit
extension RegisterVC : UITableViewDelegate , UITableViewDataSource   {
  
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.ArrayofTimes.count + 1

         
      
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "registerCell") as! registerCell
        
        
        
        cell.dateSelect = {
            let vc = Storyboard.Main.viewController(SelectDayVC.self)
            vc.delgate = cell
            
            self.present(vc, animated: true, completion: nil)
        }
        cell.fromTimeSelect = {
            let vc = Storyboard.Main.viewController(selectTime2VC.self)
            vc.delgate = cell
            
            self.present(vc, animated: true, completion: nil)
        }
        cell.toTimeSelect = {
            let vc = Storyboard.Main.viewController(selectTimeVC.self)
            vc.delgate = cell
            cell.toText.text = self.selectTimeTo
           // self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            self.present(vc, animated: true, completion: nil)
            
        }
        
        
        
        
        cell.ADDSelect = {
            
            if cell.dayText.text == "" || cell.toText.text == "" || cell.fromText.text == "" {
                showWarningAlert(title: "missed fields", message: "please fill tiems fields first!")
                              
          
            } else {
              
                self.selectedArrayofTimes.append(timeModel(day: cell.dayText.text!, from: cell.selectFromFOrmate , to: cell.selectToFormate))
                
                    self.ArrayofTimes.append(timeModel(day: cell.dayText.text!, from: cell.selectFromFOrmate , to: cell.selectToFormate)
                   
                    )
                    self.tableView.reloadData()
            }
            
            
        }
        
        
        
        
        
        
        
        
        return cell
    }
    
    
   
    
    

    
  
    
    
}
