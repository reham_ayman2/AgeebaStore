//
//  registerVC+presenterDelegete.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 11/10/2021.
//

import UIKit
import SKActivityIndicatorView

extension RegisterVC : RegisterView  , sendDataBackDelegate  , selectCategorieBack{
    func showWarningALERT(aleert: String) {
       showWarningAlert(title: "", message: aleert)
    }
    
    func showCommeriaclImagePicker() {
        commericalPicker.sourceType = UIImagePickerController.SourceType.savedPhotosAlbum
        commericalPicker.allowsEditing = true
        commericalPicker.delegate = self

      self.present(commericalPicker, animated: false, completion: nil)
    }
    
    func selectedCategory(Slug: String, name: String) {
        self.sectionText.text = name
        self.categoriSlug = Slug
        
    }
    
    
    func goToNextStep() {
        let vc = Storyboard.Main.viewController(NextRegsterVC.self)
        // pass params
        
        vc.arabicName = self.nameARText.text ?? ""
        vc.englishName = self.nameENtext.text ?? ""
        vc.categoriSlug = self.categoriSlug
        vc.lat = self.lat
        vc.long = self.long
        vc.website = self.Websitetext.text ?? ""
        vc.commericalNum = self.commericalRegNoText.text ?? ""
        vc.uploadImge = self.uploadImge
        vc.ArrayofTimes = self.ArrayofTimes
        
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
    }
    
    func showCoverPicker() {
        coverPicker.sourceType = UIImagePickerController.SourceType.savedPhotosAlbum
        coverPicker.allowsEditing = true
        coverPicker.delegate = self

      self.present(coverPicker, animated: false, completion: nil)
    }
    
    

    
    func showImagePicker() {
        userPicker.sourceType = UIImagePickerController.SourceType.savedPhotosAlbum
        userPicker.allowsEditing = true
        userPicker.delegate = self

      self.present(userPicker, animated: false, completion: nil)
    }
    
    func finishPassing(location: String, lat: Double, lng: Double) {
        self.LocationText.text = location
        self.lat = lat
        self.long = lng
    }
    
    func showIndicator() {
        SKActivityIndicator.show()
    }
    
    func hideIndicator() {
        SKActivityIndicator.dismiss()
    }
    
    func showError(error: String) {
        showErrorAlert(title: "", message: error)
    }
    
    
    
    func goTOhome() {
        
    }
    
    
    
    func goToSelectSection() {
        let vc = Storyboard.Main.viewController(selectSectionVC.self)
        vc.delgate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    func makeImageRound() {
        self.logoImage.MakeRounded()
    }
    
    
    func goSelectLocation() {
        let vc = Storyboard.Main.viewController(SelectLocationViewController.self)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func HandelTabel() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    
    
    
    
}
