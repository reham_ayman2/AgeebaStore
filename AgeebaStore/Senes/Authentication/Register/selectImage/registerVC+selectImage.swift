//
//  registerVC+selectImage.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 27/10/2021.
//

import UIKit

extension RegisterVC : UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    // user image
  

    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
     
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {

            if picker == userPicker {
                self.logoImage.image = image
               uploadImge.append(UploadData(data: (image).pngData()! as Data, fileName: "Image.jpg", mimeType: "image/jpg", name: "icon"))
                
            } else if picker == coverPicker {
                self.coverPhotoImageC.image = image
                uploadImge.append(UploadData(data: (image).pngData()! as Data, fileName: "Image.jpg", mimeType: "image/jpg", name: "cover"))
                // upload image
            } else if picker == commericalPicker {
                
                self.commericalRegCamImage.image = image
                uploadImge.append(UploadData(data: (image).pngData()! as Data, fileName: "Image.jpg", mimeType: "image/jpg", name: "commercial_image"))
                
            }
            picker.dismiss(animated: true, completion: nil)
            
            
            
        }
         
        }
    
    
    

    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
}

