//
//  selectTime2VC.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 13/10/2021.
//



import UIKit
protocol selecttime2 {
    func selectedtime2(date:String , dateFormmater : String)
    }



class selectTime2VC: UIViewController {
    var delgate:selecttime2!

    @IBOutlet weak var picker: UIDatePicker!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

     
    }
    

    @IBAction func timePicker(_ sender: UIDatePicker) {
        
        }

    @IBAction func confirm(_ sender: UIButton) {
        //hh:mm a
      
       
        let timeFormatter = DateFormatter()
        timeFormatter.dateStyle = .none
        timeFormatter.dateFormat = "hh:mm a"
        let timeFormatter2 = DateFormatter()
        timeFormatter2.dateStyle = .none
        timeFormatter2.dateFormat = "HH:mm:ss"
        
        self.delgate.selectedtime2(date:  timeFormatter.string(from:picker.date), dateFormmater: timeFormatter2.string(from: picker.date))
        
        
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func cansel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
