//
//  SelectDayVC.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 13/10/2021.
//

import UIKit

protocol selectDay {
        func selectedDay(date:String)
    }

class SelectDayVC: UIViewController , UIPickerViewDelegate , UIPickerViewDataSource {
  
    var delgate:selectDay!
    @IBOutlet weak var pickerOutlet: UIPickerView!
    
    let days = ["Saturday" , "Sunday" , "Monday" , "Tuesday" , "Wednesday" , "Thursday" , "Friday" ]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerOutlet.delegate = self
        pickerOutlet.dataSource = self

    }
    

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
      }
      
    
      func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.days.count
      }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.days[row]
    }
    @IBOutlet weak var pickerAction: UIPickerView!
    
    @IBAction func confirm(_ sender: UIButton) {
        
        
       self.delgate.selectedDay(date: days[pickerOutlet.selectedRow(inComponent: 0)])
        self.dismiss(animated: true, completion: nil)
        

    }
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
