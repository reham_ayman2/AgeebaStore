//
//  RegisterVC.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 11/10/2021.
//

import UIKit

class RegisterVC: UIViewController {
    //MARK: - VARS & CONST
    var presenter : RegisterVcPresnter!
    var selectTimeTo = ""
    var selectDay = ""
    var uploadImge = [UploadData]()
    let userPicker = UIImagePickerController()
    let commericalPicker = UIImagePickerController()
    let coverPicker = UIImagePickerController()
    var ArrayofTimes = [timeModel]()
    var categoriSlug = ""
    var selectedArrayofTimes = [timeModel]()
    var array = [String]()
    var lat = 0.0
    var long = 0.0  
  
    
    
       
        
        //MARK: - OUTLETS
    
    @IBOutlet weak var LocationText: UITextField!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var coverPhotoImageC: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nameARText: UITextField!
    @IBOutlet weak var nameENtext: UITextField!
    @IBOutlet weak var sectionText: UITextField!
    @IBOutlet weak var Websitetext: UITextField!
    @IBOutlet weak var commericalRegNoText: UITextField!
    @IBOutlet weak var commericalRegCamImage: UIImageView!
    
        
        
        //MARK: - APP CYCLE
        
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = RegisterVcPresnter(view: self)
        presenter.viewDidLoad()

    }
    

    //MARK: - ACTIONS
 
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func storePhotoPressed(_ sender: UIButton) {
        presenter.showImgaePicker()
       
    }
    @IBAction func coverPhotoPressed(_ sender: UIButton) {
        presenter.showCoverPicker()
    }
    
    @IBAction func commericalRegPhotoPressed(_ sender: UIButton) {
        presenter.showCommericalPicker()
    }
    
    @IBAction func locationPressed(_ sender: UIButton) {
        self.presenter.goSelectLocationVC()
    }
    
    @IBAction func selectSection(_ sender: UIButton) {
        self.presenter.goSelectSection()
    }
    
    
    @IBAction func nextStep(_ sender: UIButton) {
        // check and go to next page
        
  
        
        presenter.cheackData(nameAR: self.nameARText.text ?? "" , nameEN: self.nameENtext.text ?? "" , section: self.sectionText.text ?? "" , CommercialNum: self.commericalRegNoText.text ?? "" , website: self.Websitetext.text ?? "" , location: self.LocationText.text ?? "" )
        
        
        }
        
        
    
    
    
    
}
