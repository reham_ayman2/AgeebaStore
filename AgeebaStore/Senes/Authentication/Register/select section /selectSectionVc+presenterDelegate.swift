//
//  selectSectionVc+presenterDelegate.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 03/11/2021.
//


import UIKit
import SKActivityIndicatorView

extension selectSectionVC : selectSectionView , UIPickerViewDelegate , UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.ArrayofCategories.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return
            ArrayofCategories[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.selectedName = self.ArrayofCategories[row].name
        self.selectedSlug = self.ArrayofCategories[row].slug
        
        
        
    }
    
    
    
    func showIndicator() {
        SKActivityIndicator.spinnerColor(UIColor.link)
        SKActivityIndicator.show()
        
        
    }
    
    func hideIndicator() {
        
        SKActivityIndicator.dismiss()
        
    }
    
    func showError(error: String) {
        showErrorAlert(title: "", message: error)
        
    }
    
    func setData(data: [categorisData]) {
        
        self.ArrayofCategories = data
        
        picker.reloadAllComponents()
        print("🛼\(self.ArrayofCategories)")
      
    }
    
    func pickerSetup() {
        picker.delegate = self
        picker.dataSource = self
       
    }
    
    
    
    
    
}
