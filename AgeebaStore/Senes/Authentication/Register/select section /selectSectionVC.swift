//
//  selectSectionVC.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 03/11/2021.
//

import UIKit
protocol selectCategorieBack {
    func selectedCategory(Slug : String , name : String)
    }

class selectSectionVC: UIViewController {

    
    //MARK: - VARS & CONST
    var presenter : selectSectionVcPresnter!
    var ArrayofCategories = [categorisData]()
    var delgate:selectCategorieBack!
    var selectedSlug = ""
    var selectedName = "" 
      
       
       //MARK: - OUTLETS
       
    @IBOutlet weak var picker: UIPickerView!
    
       //MARK: - APP CYCLE
       
       
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = selectSectionVcPresnter(view: self)
        presenter.viewDidLoad()
        
    }
    

    //MARK: - ACTIONS
  
    @IBAction func cancel(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func confirm(_ sender: UIButton) {
        self.delgate.selectedCategory(Slug: self.selectedSlug, name: self.selectedName)
        self.dismiss(animated: true, completion: nil)
    }
    
}
