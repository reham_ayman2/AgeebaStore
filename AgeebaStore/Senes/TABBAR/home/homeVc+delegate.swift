//
//  homeVc+delegate.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 04/11/2021.
//

import Foundation
import SKActivityIndicatorView


extension homeVC : HomeView {
    func showIndicator() {
        SKActivityIndicator.show()
    }
    
    func hideIndicator() {
        SKActivityIndicator.dismiss()
    }
    
    func showError(error: String) {
        showErrorAlert(title: "", message: error)
    }
    
    func setData(data: HomeData) {
        
        self.currentOrdrs.text = String( data.num_inprogress_orders)
        self.finishedOrders.text = String ( data.num_finished_orders)
        self.newOrders.text = String ( data.num_open_orders)
        self.productNumber.text = String( data.num_products)
        
        self.view.layoutIfNeeded()
        
        
    }
    
    func gotoNotification() {
        let vc = Storyboard.Main.viewController(NotificationsVC.self)
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    
}
