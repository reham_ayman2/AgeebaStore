//
//  homeVC.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 04/11/2021.
//

import UIKit

class homeVC: UIViewController {

    //MARK: - VARS & CONST
    var presenter : HomeVcPresnter!
      
       
       //MARK: - OUTLETS
       
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var newOrders: UILabel!
    @IBOutlet weak var currentOrdrs: UILabel!
    @IBOutlet weak var productNumber: UILabel!
    @IBOutlet weak var finishedOrders: UILabel!
   
    
       //MARK: - APP CYCLE
       
      
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter = HomeVcPresnter(view: self)
        self.backView.layer.applySketchShadow()

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
        self.presenter.viewWillAppeare()
        
    }
    
    
    
    
    //MARK: - ACTIONS

    @IBAction func notifications(_ sender: UIButton) {
        self.gotoNotification()
    }
}
