//
//  myordersvc+tableview.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 05/11/2021.
//

import UIKit


extension MyOrdersVC : UITableViewDelegate , UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
            return presenter.getFinishedCellsCount()
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myOrdersCell", for: indexPath) as! myOrdersCell
       
       
            presenter.configureFinishedCells(cell: cell, for: indexPath.row)
        
        
        
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.didSelectRow(index: indexPath.row)
    }
    
    
    
}
