//
//  MyOrdersVC.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 05/11/2021.
//

import UIKit

class MyOrdersVC: UIViewController {

    
    //MARK: - VARS & CONST
     
    var presenter : MyordersVcPresnter!
   
    
      
      //MARK: - OUTLETS
    @IBOutlet weak var hedierView: UIView!
    @IBOutlet weak var segmentOutlet: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    
    
    
    
      //MARK: - APP CYCLE
      
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = MyordersVcPresnter(view: self)
        segmentOutlet.setTitle("New Orders".localized, forSegmentAt: 0)
        segmentOutlet.setTitle("Acive Orders".localized, forSegmentAt: 1)
        segmentOutlet.setTitle("Finish Orders".localized, forSegmentAt: 2)
        self.presenter.viewDidLoad()
       

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // presenter.viewWillAppeare()
        if segmentOutlet.selectedSegmentIndex == 0 {
            presenter.getOpenOrders()
        }
    }
    
    //MARK: - ACTIONS

    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 2 {
            
          
            presenter.getFinishedOrders()

        } else if sender.selectedSegmentIndex == 1 {
           
            presenter.getActiveOrders()
            
        } else {
            presenter.getOpenOrders()
            
            
        }
        
    }
    
}
