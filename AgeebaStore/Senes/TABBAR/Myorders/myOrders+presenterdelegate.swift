//
//  myOrders+presenterdelegate.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 05/11/2021.
//

import Foundation
import SKActivityIndicatorView

extension MyOrdersVC : myOrdersView {
    func FetchData() {
        self.tableView.reloadData()
    }
    func setSelected() {
        self.segmentOutlet.selectedSegmentIndex = 0
        self.view.setNeedsLayout()
    }
    
    func goToOrderDetails(id: Int) {
        let vc = Storyboard.Main.viewController(orderDetails.self)
        vc.orderID = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func showIndicator() {
        SKActivityIndicator.show()
    }
    
    func hideIndicator() {
        SKActivityIndicator.dismiss()
    }
    
    func showError(error: String) {
        showErrorAlert(title: "", message: error)
    }
    
    func handelTable() {
        tableView.delegate = self
        tableView.dataSource = self
        self.hedierView.layer.applySketchShadow()
    }
    
    
    
}
