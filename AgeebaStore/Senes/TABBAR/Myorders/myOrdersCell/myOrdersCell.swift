//
//  myOrdersCell.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 05/11/2021.
//

import UIKit

class myOrdersCell: UITableViewCell , OrdersCellsView{
    
    
    
    func setToLocation(Location: String) {
        self.location.text = Location
    }
    
    func SetDate(date: String) {
        self.time.text = date
    }
    
    func SetUserName(userName: String) {
        self.userName.text = userName
    }
    
    func setRequestNum(num: Int) {
        self.requestNum.text = String(num)
    }
    
    func setDetails(details: String ) {
        self.details.text = details
    }
    
    func setImage(image: String) {
        self.userImage.setImageWith(image)
    }
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var requestNum: UILabel!
    @IBOutlet weak var location: UILabel!
    
    @IBOutlet weak var time: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

 

}
