//
//  selectCityViewController.swift
//  Pingek
//
//  Created by Sara Ashraf on 4/18/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//


import UIKit
import BottomPopup
import Foundation

protocol selectCity {
    func selectCity(iso:String,key:String,isCity:Bool,name:String)
}

class selectCityViewController: BottomPopupViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var categoryPicker: UIPickerView!
    
    
    var isCity:Bool?
    var isGender:Bool?
    var sectionArray = [Country]()
    var cities = [City]()
    var gender = ["male" , "female"]
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    var delegate: selectCity?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.topView.layer.cornerRadius = 4
        self.topView.clipsToBounds = true
        self.categoryPicker.delegate = self
        self.categoryPicker.dataSource = self
        if isCity == true {
            titleLabel.text = "Select City".localized()
        } else {
            if isGender == true {
                titleLabel.text = "Select Gender".localized()
            } else {
                titleLabel.text = "Select Country".localized()
            }
        }
        
    }
    
    
    @IBAction func confirmDateandTime(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    //MARK: - Pickerview method
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if isCity == true {
            return cities.count
        } else {
            if isGender == true {
                return gender.count
            } else {
                return sectionArray.count
            }
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var result:String = ""
        
        if isCity == true {
            result = cities[row].name
        } else {
            if isGender == true {
                result = gender[row].localized()
            } else {
                result = sectionArray[row].iso + "(\(sectionArray[row].key))"
            }
        }
        return result
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if isCity == true {
            if cities.isEmpty {
                self.view.endEditing(true)
            } else {
                self.delegate?.selectCity(iso: String(cities[row].id), key: cities[row].name, isCity: true, name: cities[row].name)
            }
        } else {
          
            if(isGender == false){
                self.delegate?.selectCity(iso: sectionArray[row].iso, key: sectionArray[row].key, isCity: false, name: sectionArray[row].name)

            }else{
                self.delegate?.selectCity(iso:"", key: gender[row], isCity: false, name: gender[row])

            }
           
        }
        
        
    }
    
    
    //MARK: - PopUp Configration
    func getPopupHeight() -> CGFloat {
        return height ?? CGFloat(300)
    }
    
    func getPopupTopCornerRadius() -> CGFloat {
        return topCornerRadius ?? CGFloat(10)
    }
    
    func getPopupPresentDuration() -> Double {
        return presentDuration ?? 1.0
    }
    
    func getPopupDismissDuration() -> Double {
        return dismissDuration ?? 1.0
    }
    
    func shouldPopupDismissInteractivelty() -> Bool {
        return shouldDismissInteractivelty ?? true
    }
    
    
    
}
