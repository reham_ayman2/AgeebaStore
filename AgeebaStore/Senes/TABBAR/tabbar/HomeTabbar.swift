//
//  HomeTabbar.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 04/11/2021.
//

import UIKit

class HomeTabbar: UITabBarController {

 

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Cairo-SemiBold", size: 12)!], for: .normal)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name:  "Cairo-SemiBold", size: 15)!]
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Cairo-SemiBold", size: 15)!], for: UIControl.State.normal)
        
        tabBar.items![0].title = "Home".localized
        tabBar.items![1].title = "My Orders".localized
        tabBar.items![2].title = "My Products".localized
        tabBar.items![3].title = "More".localized
      
        
        
    }
}
