//
//  NotificationsVC.swift
//  Sandkom
//
//  Created by Sara Ashraf on 1/16/20.
//  Copyright © 2020 Sara Ashraf. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SKActivityIndicatorView


class NotificationsVC: UIViewController {
   
 
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noNotfiLbl: UILabel!
    
    var NotficationDataSourse = [NotficationsModel]()
    var bidID = ""
    var pageCount = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.showsVerticalScrollIndicator = false
        self.noNotfiLbl.isHidden = true
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        self.tabBarController?.navigationItem.hidesBackButton = true
        self.tableView.refreshControl = refreshControl
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.tableFooterView = UIView()
        
        tableView.register(UINib(nibName: "NotficationsCellTableViewCell", bundle: nil), forCellReuseIdentifier: "NotficationsCellTableViewCell")
        
        tableView.animateTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        SocketConnection.sharedInstance.socket.off("message")
        SocketConnection.sharedInstance.socket.emit("existChat", with: []) {
            print("added user id \(User.currentUser?.id)")
        }
        self.tabBarController?.tabBar.items![2].badgeValue = nil
        self.tabBarController?.tabBar.items![3].badgeValue = nil
        self.tabBarController?.tabBar.isHidden = false
        self.pageCount = 1
        self.NotficationDataSourse = []
        self.tableView.reloadData()
        SKActivityIndicator.show()
        getNotfications(page:self.pageCount)
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        SKActivityIndicator.show()
        self.deleteAllNotifications()
    }
    
    
    func deleteAllNotifications(){
        self.tableView.reloadData()
       let header:HTTPHeaders = [
        "Authorization":"Bearer" + User.currentUser!.token  ,
            "lang":getServerLang()
        ]
        API.POST(url: DeleteAllNotification_URL, parameters: [:], headers: header) { (succss, value) in
            print("🌸\(value)")
            if succss{
                SKActivityIndicator.dismiss()
                let key = value["key"] as? String
                if(key == "success"){
                    self.NotficationDataSourse.removeAll()
                    self.tableView.reloadData()
                    self.getNotfications(page: self.pageCount)
                }else{
                    let msg = value["msg"] as? String
                    showErrorAlert(title: "Error", message: msg ?? "" )
                    //ShowErrorMassge(massge: msg!, title: "Error".localized)
                }
                if self.NotficationDataSourse.count == 0 {
                    self.noNotfiLbl.isHidden = false
            
                }else{
                    self.noNotfiLbl.isHidden = true
                 
                   
                }
                
                
            }else{
                SKActivityIndicator.dismiss()
                
                
            }
            
        }
        
    }
    //==============refrresh controller
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:#selector(handleRefresh(_:)),for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.BasicColor
        
        return refreshControl
    }()
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        getNotfications(page:self.pageCount)
        refreshControl.endRefreshing()
    }
    
    
    
    
}



extension NotificationsVC{
    
    func getNotfications(page:Int){
        self.NotficationDataSourse.removeAll()
        self.tableView.reloadData()
        let parmas :Parameters = [
            "page": page
            
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + User.currentUser!.token,
            "lang":getServerLang()
        ]
        API.POST(url: Notficatios_URL, parameters: parmas, headers: header) { (succss, value) in
            print("🤭\(parmas)")
            print("🌸\(value)")
            if succss{
                SKActivityIndicator.dismiss()
                let key = value["key"] as? String
                
                if(key == "success"){
                    let data = value["data"] as! [[String:Any]]
                    if(data.count == 0 && self.NotficationDataSourse.count == 0){
                        self.noNotfiLbl.isHidden = false
                        return
                    }else{
                        for i in data{
                            self.noNotfiLbl.isHidden = true
                            let user_id = i["user_id"] as! Int
                            let id = i["id"] as! Int
                            let name = i["name"] as! String
                            let image = i["image"] as! String
                            let text = i["text"] as! String
                            let order_status = i["order_status"] as! String
                            let seen = i["seen"] as! String
                            let date = i["date"] as! String
                            let conversation_id = i["conversation_id"] as? Int ?? 0
                            let key = i["key"] as! String
                            // let bid_id = i["bid_id"] as? String ?? ""
                            let dataID = i["data"] as! [String:Any]
                            
                            switch key {
                            case "applyBid":
                                ///بتوديني علي صفحه القبول او الرفض
                                let bid_id = dataID["bid_id"] as! String
                                print("bid_id\(bid_id)")
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:bid_id, orderRoute:key, place_ref: "", place_id: "", conversation_id: conversation_id, order_id: ""))
                                
                            case "newOrder":
                                //بتوديني على صفحه الطلبات القريبه مني بصفه عامه
                                let order_id = dataID["order_id"] as! String
                                print("order_id\(order_id)")
                                var place_id = ""
                                var place_ref = ""
                                if dataID["place_id"] as? String != nil {
                                    place_id = dataID["place_id"] as! String
                                }
                                if dataID["place_id"] as? String != nil {
                                    place_id = dataID["place_id"] as! String
                                }
                                if dataID["store_id"] as? String != nil {
                                    place_id = dataID["store_id"] as! String
                                }
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:order_id, orderRoute:key, place_ref: place_ref, place_id: place_id, conversation_id: conversation_id, order_id: ""))
                                
                            case "agreeBid":
                                //بتوديني على صفخه الشات
                                let conversationagree_id = dataID["conversation_id"] as! String
                                print("conversation_id\(conversation_id)")
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:String(conversationagree_id), orderRoute:key, place_ref: "", place_id: "", conversation_id: conversation_id, order_id: ""))
                                
                            case "newOrderwithPlace":
                                //بتوديني على صفحه الطلبات القريبه الي خاصه بمكان معين
                                let place_id = dataID["place_id"] as! String
                                let place_ref = dataID["place_ref"] as! String
                                print("newOrderwithPlace)")
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text, order_status: order_status, seen: seen, date: date, orderNextPage: "", orderRoute: key, place_ref: place_ref, place_id: place_id, conversation_id: conversation_id, order_id: ""))
                            case "ratingYou":
                                //بتودني على صفحه الكومنتات
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:"", orderRoute:key, place_ref: "", place_id: "", conversation_id: conversation_id, order_id: ""))
                                
                            case "gotTheOrder":
                                //بتوديني على صفخه الشات
                                let conversationagree_id = dataID["conversation_id"] as? String ?? ""
                                let order_id = dataID["order_id"] as? String ?? ""
                                
                                print("conversation_id\(conversation_id)")
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:String(conversationagree_id), orderRoute:key, place_ref: "", place_id: "", conversation_id: conversation_id, order_id: order_id))
                                
                                
                                
                            case "reachedLocation":
                                //بتوديني على صفخه الشات
                                let conversationagree_id = dataID["conversation_id"] as? String ?? ""
                                let order_id = dataID["order_id"] as? String ?? ""
                                
                                print("conversation_id\(conversation_id)")
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:String(conversationagree_id), orderRoute:key, place_ref: "", place_id: "", conversation_id: conversation_id, order_id: order_id))
                                
                                
                            case "order_cash":
                                //بتوديني على صفخه الشات
                                let conversationagree_id = dataID["conversation_id"] as? String ?? ""
                                let order_id = dataID["order_id"] as? String ?? ""
                                
                                print("conversation_id\(conversation_id)")
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:String(conversationagree_id), orderRoute:key, place_ref: "", place_id: "", conversation_id: conversation_id, order_id: order_id))
                                
                                
                            case "gotCash":
                                //بتوديني على صفخه الشات
                                let conversationagree_id = dataID["conversation_id"] as? String ?? ""
                                let order_id = dataID["order_id"] as? String ?? ""
                                
                                print("conversation_id\(conversation_id)")
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:String(conversationagree_id), orderRoute:key, place_ref: "", place_id: "", conversation_id: conversation_id, order_id: order_id))
                                
                                
                                
                            case "receiveTheRequest":
                                //بتوديني على صفخه الشات
                                let conversationagree_id = dataID["conversation_id"] as? String ?? ""
                                let order_id = dataID["order_id"] as? String ?? ""
                                
                                print("conversation_id\(conversation_id)")
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:String(conversationagree_id), orderRoute:key, place_ref: "", place_id: "", conversation_id: conversation_id, order_id: order_id))
                                
                                
                                
                            case "reachedStore":
                                //بتوديني على صفخه الشات
                                let conversationagree_id = dataID["conversation_id"] as? String ?? ""
                                let order_id = dataID["order_id"] as? String ?? ""
                                
                                print("conversation_id\(conversation_id)")
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:String(conversationagree_id), orderRoute:key, place_ref: "", place_id: "", conversation_id: conversation_id, order_id: order_id))
                                
                            default:
                                var order_id = ""
                                if dataID["order_id"] as? String != nil {
                                    order_id = dataID["order_id"] as! String
                                }
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:order_id, orderRoute:key, place_ref: "", place_id: "", conversation_id: conversation_id, order_id: ""))
                            }
                            
                            
                        }
                        print("💙\(self.NotficationDataSourse.count)")
                        self.tableView.reloadData()
                    }
                }
                
            }else{
                SKActivityIndicator.dismiss()
                
            }
        }
        
    }
    
    func deleteNotifications(index : Int){
        self.tableView.reloadData()
        let parmas :Parameters = [
            "id": self.NotficationDataSourse[index].id
            
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + User.currentUser!.token,
            "lang":getServerLang()
        ]
        API.POST(url: NotficatiosDelete_URL, parameters: parmas, headers: header) { (succss, value) in
            print("🤭\(parmas)")
            print("🌸\(value)")
            if succss{
                SKActivityIndicator.dismiss()
                let key = value["key"] as? String
                if(key == "success"){
                    self.NotficationDataSourse.remove(at: index)
                    self.tableView.reloadData()
                }else{
                    let msg = value["msg"] as? String
                    //ShowErrorMassge(massge: msg!, title: "Error".localized)
                    showErrorAlert(title: "", message: msg  ?? "" )
                }
                if self.NotficationDataSourse.count == 0 {
                    self.noNotfiLbl.isHidden = false
                }else{
                    self.noNotfiLbl.isHidden = true
                }
                
                
            }else{
                SKActivityIndicator.dismiss()
                
                
            }
            
        }
        
    }
    
}





extension NotificationsVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return NotficationDataSourse.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotficationsCellTableViewCell", for: indexPath) as! NotficationsCellTableViewCell
        if(self.NotficationDataSourse.count != 0){
            let item = self.NotficationDataSourse[indexPath.row]
            cell.configureCell(notfi: item)
        }
        return cell
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("🌷🌷🌷\(self.NotficationDataSourse[indexPath.row].orderRoute)")
//        switch self.NotficationDataSourse[indexPath.row].orderRoute {
//        case "applyBid":
//            if(self.NotficationDataSourse[indexPath.row].conversation_id == 0){
//                let vc = Storyboard.Home.viewController(OffersBidAlert.self)
//                vc.BidId = self.NotficationDataSourse[indexPath.row].orderNextPage
//                vc.vc = self
//                vc.delegate = self
//                vc.modalPresentationStyle = .overCurrentContext
//                 vc.modalTransitionStyle = .crossDissolve
//
//                self.present(vc, animated: true)
//            }else{
//                let vc = Storyboard.Home.viewController(ChatViewController.self)
//                vc.ConvrstionId = String(self.NotficationDataSourse[indexPath.row].conversation_id)
//                self.navigationController?.pushViewController(vc, animated: false)
//            }
//        case "agreeBid":
//
//            let vc = Storyboard.Home.viewController(ChatViewController.self)
//
//            vc.ConvrstionId = self.NotficationDataSourse[indexPath.row].orderNextPage
//            self.navigationController?.pushViewController(vc, animated: false)
//        case "ratingYou":
//            let vc = Storyboard.Home.viewController(CustomrComentsViewController.self)
//            self.navigationController?.pushViewController(vc, animated: false)
//
//        case "gotTheOrder":
//
//            let vc = Storyboard.Home.viewController(ChatViewController.self)
//
//            vc.ConvrstionId = self.NotficationDataSourse[indexPath.section].orderNextPage
//            self.navigationController?.pushViewController(vc, animated: false)
//
//
//        case "reachedLocation":
//
//            let vc = Storyboard.Home.viewController(ChatViewController.self)
//
//            vc.ConvrstionId = self.NotficationDataSourse[indexPath.section].orderNextPage
//            self.navigationController?.pushViewController(vc, animated: false)
//
//
//        case "order_cash":
//
//            let vc = Storyboard.Home.viewController(ChatViewController.self)
//
//            vc.ConvrstionId = self.NotficationDataSourse[indexPath.section].orderNextPage
//            self.navigationController?.pushViewController(vc, animated: false)
//        case "gotCash":
//
//            let vc = Storyboard.Home.viewController(ChatViewController.self)
//
//            vc.ConvrstionId = self.NotficationDataSourse[indexPath.section].orderNextPage
//            self.navigationController?.pushViewController(vc, animated: false)
//
//        case "receiveTheRequest":
//
//
//            let vc = Storyboard.Home.viewController(ChatViewController.self)
//
//            vc.ConvrstionId = self.NotficationDataSourse[indexPath.section].orderNextPage
//            self.navigationController?.pushViewController(vc, animated: false)
//
//
//        case "reachedStore":
//
//
//            let vc = Storyboard.Home.viewController(ChatViewController.self)
//
//            vc.ConvrstionId = self.NotficationDataSourse[indexPath.section].orderNextPage
//            self.navigationController?.pushViewController(vc, animated: false)
//
//        case "newOrder":
//            let vc = Storyboard.Home.viewController(DelegateVC.self)
//            self.navigationController?.pushViewController(vc, animated: false)
////            let vc = Storyboard.Home.viewController(StoreDetailsVC.self)
////            vc.notificaion = self.NotficationDataSourse[indexPath.row]
////            self.navigationController?.pushViewController(vc, animated: false)
////            self.tabBarController?.selectedIndex = 2
//            
//            //    let vc = Storyboard.Home.viewController(DelegateVC.self)
//            //       self.navigationController?.pushViewController(vc, animated: false)
//
//        case "newOrderwithPlace":
//            print("oppen")
//            //            let vc = Storyboard.Home.viewController(StoreDetailsVC.self)
//            //            vc.notificaion = self.NotficationDataSourse[indexPath.row]
//            //            self.navigationController?.pushViewController(vc, animated: false)
//
//            let vc = Storyboard.Home.viewController(DelegateVC.self)
//            self.navigationController?.pushViewController(vc, animated: false)
//
//        default:
//            break
//        }
//
//    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(indexPath.row == self.NotficationDataSourse.count - 1){
            if(self.NotficationDataSourse.count == 10 || self.NotficationDataSourse.count > 10){
                self.pageCount  += 1
                print("🍉\( self.pageCount)")
                self.getNotfications(page: pageCount)
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = UIContextualAction(style: .normal, title: "delete"){(action, view , completion) in
            self.deleteNotifications(index: indexPath.row)
        }
        delete.backgroundColor =  #colorLiteral(red: 0.8509803922, green: 0.3529411765, blue: 0.3921568627, alpha: 1)
        
        
        delete.image = #imageLiteral(resourceName: "noun_delete")
        
        
        return UISwipeActionsConfiguration(actions :[delete])
    }
    
}
