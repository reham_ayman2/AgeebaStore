//
//  NotficationsCellTableViewCell.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/25/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
func viewDesign(view:UIView){
    view.layer.shadowColor = UIColor.gray.cgColor
    view.layer.shadowOpacity = 1
    view.layer.shadowOffset = CGSize.zero
    view.layer.shadowRadius = 5
}


class NotficationsCellTableViewCell: UITableViewCell {
    @IBOutlet weak var viewContiner: UIImageView!
    @IBOutlet weak var NotficationTitle: UILabel!
    @IBOutlet weak var notficationTime: UILabel!
    @IBOutlet weak var notficationImge: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewDesign(view: self.viewContiner)
        self.notficationImge.clipsToBounds = true
//        self.notficationImge.layer.borderColor = UIColor.BasicColor.cgColor
//        self.notficationImge.layer.borderWidth = 1
        self.selectionStyle = .none
       
        
    }
    
    func configureCell(notfi:NotficationsModel){
        self.NotficationTitle.text = notfi.text
        self.notficationTime.text = notfi.date
        //self.notficationImge.setImageWith(notfi.image)
        
        
    }
}
