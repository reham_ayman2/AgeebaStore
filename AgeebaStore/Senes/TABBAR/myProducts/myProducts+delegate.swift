//
//  myProducts+delegate.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 10/11/2021.
//

import UIKit
import SKActivityIndicatorView


extension myProductsVC : myProductsView  , UITableViewDelegate , UITableViewDataSource{
   
    
    func showIndicator() {
        SKActivityIndicator.show()
    }
    
    func hideIndicator() {
        SKActivityIndicator.dismiss()
    }
    
    func showError(error: String) {
        showErrorAlert(title: "", message: error)
    }
    
    func handelTable() {
        
        tableview.delegate = self
        tableview.dataSource = self
        
        
    }
    
    func FetchData() {
        
        self.tableview.reloadData()
        
    }
    
    func handelHeaderShadow() {
        self.hederView.layer.applySketchShadow()
        
    }
    
   //MARK: - TABLE VIEW METHODS
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.getProductsCounts()
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "myproductsCell") as! myproductsCell
        self.presenter.configureCells(cell: cell , for: indexPath.row)
        
        return cell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}



 
