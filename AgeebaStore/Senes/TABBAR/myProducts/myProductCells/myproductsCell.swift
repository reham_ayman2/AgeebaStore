//
//  myproductsCell.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 11/11/2021.
//

import UIKit

class myproductsCell: UITableViewCell , ProdusctssCellsView {
  

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var details: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        
        
    }

    func setImage(image: String) {
        self.productImage.setImageWith(image)
      }
      
      func setProductName(name: String) {
        self.productName.text = name
      }
      
      func setDetails(details: String) {
        self.details.text = details
      }
      
      func setStatus(status: String) {
        
        
        if status == "avilable" {
            self.status.text = status
            self.status.textColor = .systemGreen
        } else {
            self.status.text = status
            self.status.textColor = .darkGray
        }
        
        
      }
      
      func setCost(price: String) {
        self.price.text = price + "S.R"
      }
      

}
