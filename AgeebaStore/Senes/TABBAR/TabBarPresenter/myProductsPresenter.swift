//
//  myProductsPresenter.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 10/11/2021.
//

import Foundation
//MARK: - PROTOCOLS
protocol myProductsView: class {
    func showIndicator()
    func hideIndicator()
    func showError(error:String)
    func handelTable()
    func FetchData()
    func handelHeaderShadow()
    
    
  
  
}

protocol ProdusctssCellsView  {
    
    func setImage ( image : String)
    func setProductName ( name : String)
    func setDetails ( details : String)
    func setStatus ( status : String)
    func setCost ( price : String)
    
    
    

  
  
}



//MARK: - PRESENTER CLASS
class MyproducstsVcPresnter {
    
    private weak var view : myProductsView?
    private var products = [ProductData]()
   

    
    
    
    init(view:myProductsView) {
        self.view = view
    }
    
    
    
    func viewDidLoad () {
        self.view?.handelTable()
        self.view?.handelHeaderShadow()
    }
    
    
    
    
    
    func viewWillAppeare(){
       
        self.getMyProducts()
    }
    
    
    func getProductsCounts () -> Int {
         
        return self.products.count
        
        
    }
    
    func configureCells(cell: ProdusctssCellsView  , for index: Int) {
        
        cell.setCost(price: products[index].price ?? ""  )
        cell.setImage(image: products[index].image ?? "" )
        cell.setStatus(status: products[index].status ?? "" )
        cell.setDetails(details: products[index].description ?? "" )
        cell.setProductName(name: products[index].name ?? "" )
        
      
        
        
      
    }
    
    
  
    
    
    func getMyProducts(){
        view?.showIndicator()
        self.products.removeAll()
        AgebaInteractor.storeMyProducts.send(productModel.self){
            [weak self] (response) in

            guard let self = self else { return }
            self.view?.hideIndicator()
            switch response {
            case .unAuthorized(_):
                print("unAuthorized")
            case .failure(let error):

                print("failure\(String(describing: error))")
                
            case .success(let value):
                print(value)
                self.products = value.data!
                
                
                self.view?.FetchData()


            case .errorResponse(let error):
                guard let errorMessage = error as? APIError else { return  showNoInterNetAlert()}
                self.view?.showError(error: errorMessage.localizedDescription.localized)
                
            }
        }
    }
    
    
  
    
    
    
  
    

    
    
    
}
