//
//  myOrdersPresenter.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 05/11/2021.
//


import Foundation


import Foundation
//MARK: - PROTOCOLS
protocol myOrdersView: class {
    func showIndicator()
    func hideIndicator()
    func showError(error:String)
    func handelTable()
    func FetchData()
    func setSelected ()
    func goToOrderDetails( id : Int)
    
    
  
  
}

protocol OrdersCellsView  {
    
func setToLocation ( Location : String)
func SetDate ( date : String)
func SetUserName ( userName : String)
func setRequestNum ( num : Int)
func setDetails ( details : String )
    func setImage ( image : String )
    
  
  
}



//MARK: - PRESENTER CLASS
class MyordersVcPresnter {
    
    private weak var view: myOrdersView?
    

   var Orders = [OrdersData]()
   

    
    
    
    init(view:myOrdersView) {
        self.view = view
    }
    
    
    
    func viewDidLoad () {
        
        self.view?.handelTable()
        self.Orders.removeAll()
    }
    
    
    func viewWillAppeare(){
       
        self.getOpenOrders()
        self.view?.setSelected()
    
    }
    
    
    func getFinishedCellsCount() -> Int {
        return Orders.count
    }
    
    
    
    func configureFinishedCells(cell: OrdersCellsView  , for index: Int) {
        
        cell.SetDate(date: Orders[index].date ?? "" )
        cell.setDetails(details: Orders[index].details?.first?.value ?? "--"  )
        cell.SetUserName(userName: Orders[index].user_name ?? "" )
        cell.setToLocation(Location: Orders[index].deliver_address ?? "--" )
        cell.setRequestNum(num: Orders[index].id ?? 0 )
        cell.setImage(image: Orders[index].avatar ?? "" )
        
      
    }
    
    
    func getFinishedOrders(){
        view?.showIndicator()
        self.Orders.removeAll()
        AgebaInteractor.storeFinishedOrders.send(OrdersModel.self){
            [weak self] (response) in

            guard let self = self else { return }
            self.view?.hideIndicator()
            switch response {
            case .unAuthorized(_):
                print("unAuthorized")
            case .failure(let error):

                print("failure\(String(describing: error))")
                
            case .success(let value):
                print(value)
                self.Orders = value.data!
                
                
                self.view?.FetchData()


            case .errorResponse(let error):
                guard let errorMessage = error as? APIError else { return  showNoInterNetAlert()}
                self.view?.showError(error: errorMessage.localizedDescription.localized)
                
            }
        }
    }
    
    
    

    func getOpenOrders(){
        view?.showIndicator()
        self.Orders.removeAll()
        AgebaInteractor.storeOpenOrders.send(OrdersModel.self){
            [weak self] (response) in

            guard let self = self else { return }
            self.view?.hideIndicator()
            switch response {
            case .unAuthorized(_):
                print("unAuthorized")
            case .failure(let error):

                print("failure\(String(describing: error))")
                
            case .success(let value):
                print(value)
                self.Orders = value.data!
                
                
                self.view?.FetchData()


            case .errorResponse(let error):
                guard let errorMessage = error as? APIError else { return  showNoInterNetAlert()}
                self.view?.showError(error: errorMessage.localizedDescription.localized)
                
            }
        }
    }
    
    
    
    
    
    func getActiveOrders(){
        view?.showIndicator()
        self.Orders.removeAll()
        AgebaInteractor.storeActiveOrders.send(OrdersModel.self){
            [weak self] (response) in

            guard let self = self else { return }
            self.view?.hideIndicator()
            switch response {
            case .unAuthorized(_):
                print("unAuthorized")
            case .failure(let error):

                print("failure\(String(describing: error))")
                
            case .success(let value):
                print(value)
                self.Orders = value.data!
                
                
                self.view?.FetchData()


            case .errorResponse(let error):
                guard let errorMessage = error as? APIError else { return  showNoInterNetAlert()}
                self.view?.showError(error: errorMessage.localizedDescription.localized)
                
            }
        }
    }
    
    
    func didSelectRow (index : Int) {
      
        self.view?.goToOrderDetails(id: Orders[index].id ?? 0)
        
    }
    
    
}
