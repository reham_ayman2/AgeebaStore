//
//  HomeVCPresenter.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 04/11/2021.
//

import Foundation


import Foundation
//MARK: - PROTOCOLS
protocol HomeView: AnyObject {
    func showIndicator()
    func hideIndicator()
    func showError(error:String)
    func setData(data : HomeData)
    func gotoNotification ()
  
  
}




//MARK: - PRESENTER CLASS
class HomeVcPresnter {
    
    private weak var view: HomeView?
    
    
    
    init(view:HomeView) {
        self.view = view
    }
    
    
    func viewWillAppeare(){
        
        
        getHomeData()
        
        
     
    }
    
    
    func getHomeData(){
        view?.showIndicator()
        AgebaInteractor.storeHome.send(HomeModel.self) {
            [weak self] (response) in
            guard let self = self else { return }
            self.view?.hideIndicator()
            switch response {
            case .unAuthorized(_):
                
                print("unAuthorized")
                
            case .failure(let error):
                
                showNoInterNetAlert()
                print("failure\(String(describing: error))")
                
            case .success(let value):
                print("hii")
                
                
                self.view?.setData(data: value.data)
                
              
                
            case .errorResponse(let error):
                guard let errorMessage = error as? APIError else { return  showNoInterNetAlert()}
                self.view?.showError(error: errorMessage.localizedDescription.localized)
            }
        }

    }
    
    
    
    

    
    
}
