//
//  orderDetailsPresenter.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 11/11/2021.
//


import Foundation
//MARK: - PROTOCOLS
protocol orderDetailsView: AnyObject {
    func showIndicator()
    func hideIndicator()
    func showError(error:String)
    func handelTable()
    func FetchData()
    func handelHeaderShadow()
    func setData (data : OrderDetailsData )
    func Dissmiss ()
    
    
  
  
}

protocol orderDetailsCellsView  {
    
    func setImage ( image : String)
    func setProductName ( name : String)
    func setCost ( cost : String)
    func setQuantity ( quantity : String )
    func setSize ( size : String)
  
 
  
}



//MARK: - PRESENTER CLASS
class orderDetailsVcPresnter {
    
    private weak var view : orderDetailsView?
    private var products = [Products]()
   

    
    
    
    init(view:orderDetailsView) {
        self.view = view
    }
    
    
    
    func viewDidLoad () {
        self.view?.handelTable()
        self.view?.handelHeaderShadow()
       
        
    }
    
    
    func configureCells(cell: orderDetailsCellsView  , for index: Int) {
        cell.setCost(cost: self.products[index].price ?? "" )
        cell.setProductName(name: self.products[index].name ?? "" )
        cell.setImage(image: self.products[index].image ?? ""  )
        cell.setSize(size: self.products[index].size ?? "" )
        cell.setQuantity(quantity: self.products[index].count ?? "" )
        
        
    }
    
    func getCellsCount () -> Int {
        return self.products.count
    }
    
    func getOrderDetails(orderId : Int , lat : Double , long : Double){
        view?.showIndicator()
        self.products.removeAll()
        AgebaInteractor.storeOrderDetails(Orderid: orderId, lat: lat, long: long ).send(orderDetailsModel.self){
            [weak self] (response) in

            guard let self = self else { return }
            self.view?.hideIndicator()
            switch response {
            case .unAuthorized(_):
                print("unAuthorized")
            case .failure(let error):
                print("failure\(String(describing: error))")
            case .success(let value):
                
                
                self.view?.setData(data: value.data! )
                self.products  = value.data?.products ?? []
                self.view?.FetchData()
              
            case .errorResponse(let error):
                guard let errorMessage = error as? APIError else { return  showNoInterNetAlert()}
                self.view?.showError(error: errorMessage.localizedDescription.localized)
            }
        }

    }
    
    
    func refeueOrder (orderId  : Int , long : Double ) {
        
     
            view?.showIndicator()
            self.products.removeAll()
            AgebaInteractor.storeCancelOrder(order_id: orderId, long: long).send(sendDefualtModel.self){
                [weak self] (response) in

                guard let self = self else { return }
                self.view?.hideIndicator()
                switch response {
                case .unAuthorized(_):
                    print("unAuthorized")
                case .failure(let error):
                    print("failure\(String(describing: error))")
                case .success(let value):
                    
                    showSuccessAlert(title: "", message: value.msg)
                    self.view?.Dissmiss()
                    
                  
                case .errorResponse(let error):
                    guard let errorMessage = error as? APIError else { return  showNoInterNetAlert()}
                    self.view?.showError(error: errorMessage.localizedDescription.localized)
                }
            }

        }
        
    
    func AcceptOrder (orderId  : Int , long : Double ) {
        
     
            view?.showIndicator()
            self.products.removeAll()
            AgebaInteractor.storeAcceptOrder(order_id: orderId, long: long).send(sendDefualtModel.self){
                [weak self] (response) in

                guard let self = self else { return }
                self.view?.hideIndicator()
                switch response {
                case .unAuthorized(_):
                    print("unAuthorized")
                case .failure(let error):
                    print("failure\(String(describing: error))")
                case .success(let value):
                    
                    showSuccessAlert(title: "", message: value.msg)
                    self.view?.Dissmiss()
                    
                  
                case .errorResponse(let error):
                    guard let errorMessage = error as? APIError else { return  showNoInterNetAlert()}
                    self.view?.showError(error: errorMessage.localizedDescription.localized)
                }
            }

        }
        
    
    
    
    
}
