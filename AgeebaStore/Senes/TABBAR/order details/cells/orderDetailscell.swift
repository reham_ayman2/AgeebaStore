//
//  orderDetailscell.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 12/11/2021.
//

import UIKit

class orderDetailscell: UITableViewCell  , orderDetailsCellsView{
   
    
  
    
   
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var cost: UILabel!
    @IBOutlet weak var sizeee: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setQuantity(quantity: String ) {
        self.quantity.text = "Quantity: " + String( quantity)

       }
    
    
    
    func setImage(image: String) {
        self.productImage.setImageWith(image)
      }
      
      func setProductName(name: String) {
        self.productName.text = name
      }
      
      func setCost(cost: String) {
        self.cost.text = cost
      }
   
      
      func setSize(size: String) {
        self.sizeee.text = "Size: " + size
      }
  

}
