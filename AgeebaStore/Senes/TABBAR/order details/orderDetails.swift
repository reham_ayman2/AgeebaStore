//
//  orderDetails.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 11/11/2021.
//

import UIKit
import MBRadioCheckboxButton

class orderDetails: UIViewController {
    @IBOutlet weak var prep: UIButton!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var cost: UILabel!
    @IBOutlet weak var orderDetails: UITextView!
    @IBOutlet weak var deleegateView: UIView!
    @IBOutlet weak var accedtStack: UIStackView!
    @IBOutlet weak var delegateName: UILabel!
    @IBOutlet weak var delegateNum: UILabel!
    @IBOutlet weak var delegateImage: UIImageView!
    @IBOutlet weak var requestDate: UILabel!
    @IBOutlet weak var paymentStatus: UILabel!
    @IBOutlet weak var clintName: UILabel!
    @IBOutlet weak var paymentMethod: UILabel!
    @IBOutlet weak var requestStatus: UILabel!
    @IBOutlet weak var requestNum: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var thirdRadioBtn: RadioButton!
    @IBOutlet weak var secondRadioBtn: RadioButton!
    @IBOutlet weak var firstRadioBTN: RadioButton!
    @IBOutlet weak var header: UIView!
    
    var orderID = 0
    var presentr : orderDetailsVcPresnter!
    var status = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
        presentr = orderDetailsVcPresnter(view: self)
        presentr.viewDidLoad()
        print(orderID)
        
        self.presentr.getOrderDetails(orderId: self.orderID, lat: User.currentUser!.lat , long: User.currentUser!.long)
    
    }
  
    

    @IBAction func notification(_ sender: UIButton) {
    }
    
    
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancelOrder(_ sender: UIButton) {
        presentr.refeueOrder(orderId: self.orderID, long: User.currentUser?.long ?? 0.0 )
        
    }
    
    @IBAction func acceptOrder(_ sender: UIButton) {
        presentr.AcceptOrder(orderId: self.orderID, long: User.currentUser?.long ?? 0.0 )
      
    }
  
    @IBAction func prepared(_ sender: UIButton) {
        
    }
}
