//
//  orderDetails+delegate.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 11/11/2021.
//

import UIKit
import SKActivityIndicatorView

// order step           status

// "finished"         "finished"
// "accepted"        "inprogress"
//  "new"              "open"


extension orderDetails : orderDetailsView , UITableViewDelegate , UITableViewDataSource
{
    func setData(data: OrderDetailsData) {
        self.orderDetails.text = data.details?.first?.value
        self.cost.text = data.price
        self.requestNum.text = String(data.id! )
        if data.needs_delivery == "true" {
            self.requestStatus.text = "Request With Delivery".localized
            self.requestStatus.backgroundColor = .link
            
        } else {
            self.requestStatus.text = "Request Without Delivery".localized
            self.requestStatus.backgroundColor = .systemGreen
            
        }
        
        self.clintName.text = data.username
        self.paymentMethod.text = data.paymentMethod
        self.paymentStatus.text = String( data.payment_status ?? 0)
        if data.needs_delivery == "true"  {
            self.deleegateView.isHidden = false
                
            // handel delegate view
           
        } else {
            self.deleegateView.isHidden = true
        }
        
        
       
        
       
        if data.status == "inprogress" {
            self.accedtStack.isHidden = true
            self.prep.isHidden = false
            
            self.handelIprogressView()
           
            
        } else if data.status == "finished" {
            self.accedtStack.isHidden = true
            self.prep.isHidden = true
            
            self.handelFinishView()
            
            
            // add here recived status first one
        } else if  data.status ==  "open" {
            
            
            if data.order_step == "new" {
                self.accedtStack.isHidden = false
                self.prep.isHidden = true
            } else {
                self.accedtStack.isHidden =  false
                self.prep.isHidden = true
                
            }
            
            
            
        }
      
        
        self.view.setNeedsLayout()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentr.getCellsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell =  tableview.dequeueReusableCell(withIdentifier: "orderDetailscell", for: indexPath) as! orderDetailscell
        
        presentr.configureCells(cell: cell, for: indexPath.row)
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func showIndicator() {
        SKActivityIndicator.show()
    }
    
    func hideIndicator() {
        SKActivityIndicator.dismiss()
    }
    
    func showError(error: String) {
        showErrorAlert(title: "", message: error)
    }
    
    func handelTable() {
        tableview.delegate = self
        tableview.dataSource = self
     
        
    }
    
    func FetchData() {
        self.tableview.reloadData()
        self.view.layoutIfNeeded()
    }
    
    func handelHeaderShadow() {
        self.header.layer.applySketchShadow()
        
    }
    
    
    func Dissmiss() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            guard let window = UIApplication.shared.keyWindow else { return }
            let vc = Storyboard.Main.viewController(StartNavBar.self)
            window.rootViewController = vc
        }
    }
    
    func handelIprogressView () {
        
        self.firstRadioBTN.isOn = true
        self.secondRadioBtn.isOn = true
        self.firstView.backgroundColor = .systemOrange
        
        
    }
    
    
    func handelFinishView () {
        self.firstView.backgroundColor = .systemOrange
        self.secondView.backgroundColor = .systemOrange
        self.firstRadioBTN.isOn = true
        self.secondRadioBtn.isOn = true
        self.thirdRadioBtn.isOn = true
        
    }
    
    
 
    
}
