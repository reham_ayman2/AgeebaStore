//
//  more+mvc.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 13/11/2021.
//

import Foundation
import Alamofire
import SKActivityIndicatorView
import SafariServices

extension moreVC : SFSafariViewControllerDelegate {
    
    func logOut () {
        
        SKActivityIndicator.show()
            let header:HTTPHeaders = [
                "Authorization":"Bearer" + User.currentUser!.token,
                "lang":getServerLang()
            ]
        
        
        let url = "https://agybh.4hoste.com/api/logout"
        
            API.POST(url: url , parameters: ["device_id":AppDelegate.FCMTOKEN], headers: header) { (succss, value) in
                if succss{
                    
                    let key = value["key"] as? String
                    if(key == "success"){
                        let msg = value["msg"] as? String
                  
                        
                        showSuccessAlert(title: "", message: "Signed out successfully")
                        KeyChain.deleteToken()
                        User.currentUser = nil
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                          
                            
                            // go to main vc 
                            
                            guard let window = UIApplication.shared.keyWindow else { return }
                            let vc = Storyboard.Main.initialViewController()
                            window.rootViewController = vc
                            
                            
                        }
                        
                        SKActivityIndicator.dismiss()
                    }
                    
                }else{
                    SKActivityIndicator.dismiss()
                    //   print("🍉")
                }
            }
    }
    func aboutUS () {
        let safariVC = SFSafariViewController(url: NSURL(string: AboutWeb_URL + "/\(getServerLang())")! as URL)
        self.present(safariVC, animated: true, completion: nil)
        safariVC.delegate = self

    }
    
    func getShareText(){
        API.POST(url: shareTitle_Url, parameters: [:], headers: ["lang":getServerLang()]) { (succss, value) in
            if succss{
                SKActivityIndicator.dismiss()
                let data = value["data"] as! [String:Any]
                //let share_title = data["share_title"] as! String
                let phone = data["phone"] as! String
              //  let whatsphone = data["whatsphone"] as! String
               // let email = data["email"] as! String
               // self.email = email
                self.phoneNumber = phone
//                self.wahtsNumber = whatsphone
//                self.shareTitle = share_title
            }else{
                SKActivityIndicator.dismiss()
                
            }
        }
    }

    
    
    
    
    

    
    func callus () {
        if let url = URL(string: "tel://\(self.phoneNumber)"),
        UIApplication.shared.canOpenURL(url) {
           if #available(iOS 10, *) {
             UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
                 // add error message here
            
            print("error ! 🔴")
            
            
        }
    }
}
