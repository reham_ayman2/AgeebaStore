//
//  moreVC+tableview.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 07/11/2021.
//

import UIKit


extension moreVC : UITableViewDelegate , UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell") as! moreCell
        cell.moreName.text = items[indexPath.row]
        cell.moreImage.image = UIImage(named: itemImages[indexPath.row])
        cell.selectionStyle = .none
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.row == 0 {
            // profile
            let Reg = Storyboard.Main.viewController(EditProfileViewController.self)
            self.navigationController?.pushViewController(Reg, animated: false)
            
        } else if indexPath.row == 1 {
            // menue section
            
            
        } else if indexPath.row == 2  {
            //sections to add product
            
        } else if indexPath.row == 3  {
            
            
            // wallet
            
            let vc = Storyboard.Main.viewController(WalletViewController.self)
            self.navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.row == 4  {
            //comment and rating
            
            
            
            
        } else if indexPath.row ==  5  {
            // about the app
            
            self.aboutUS()
            
            
            
            
        } else if indexPath.row == 6 {
            // call us
            
            self.callus()
            
            
        } else if indexPath.row == 7 {
            // cahange language
            let Reg = Storyboard.Main.viewController(LanguageSettingViewController.self)
            self.navigationController?.pushViewController(Reg, animated: false)
            
            
            
        } else if indexPath.row == 8 {
            
            // logout
            
            
            let alert = UIAlertController()
            alert.customAlert(title: "Sign Out".localized, message:"Are you sure you want to log out?".localized )
            
                
                
                
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { action in
            
                self.logOut()
            }))
            alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
            
            
            
        }
      
    }
    
    
}


