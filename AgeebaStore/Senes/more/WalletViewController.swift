//
//  WalletViewController.swift
//  Pingek
//
//  Created by Sara Ashraf on 5/17/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import SKActivityIndicatorView

class WalletViewController: UIViewController {
    @IBOutlet weak var yourBlanceLable: UILabel!
    @IBOutlet weak var paymntButton: UIButton!
    @IBOutlet var backButton: UIButton!
    
    var isGetFromSetting:Bool!
    var orderId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left") ), for: .normal)

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        SKActivityIndicator.show()
        getBlance()
    }
    

    @IBAction func paymentAction(_ sender: Any) {
        
        let BASE = "https://agybh.4hoste.com"
        guard let url = URL(string:"\(BASE)/payment/\(User.currentUser?.id)/\(AppLanguage.getLang())") else { return }
                  UIApplication.shared.open(url)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getBlance(){
        
        let yourBlance_URL = "https://agybh.4hoste.com/api/balance"
        API.POST(url: yourBlance_URL, parameters: [:], headers:  ["Authorization":"Bearer" + User.currentUser!.token,"lang":getServerLang()]) { (succss, value) in
            if succss{
                SKActivityIndicator.dismiss()
                let data = value["data"] as! String
                self.yourBlanceLable.text = data
               
            }else{
                SKActivityIndicator.dismiss()

            }
        }
    }

    
}
