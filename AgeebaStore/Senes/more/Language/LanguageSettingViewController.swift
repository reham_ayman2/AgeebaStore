//
//  LanguageSettingViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/26/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import SKActivityIndicatorView
//import ActionSheetPicker_3_0
class LanguageSettingViewController: UIViewController {
    let paragraphStyle = NSMutableParagraphStyle()
    var Language = ""
    var langArray = ["English" , "عربي"]
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var arbicView: UIView!
    @IBOutlet weak var englishView: UIView!
    @IBOutlet weak var cheackArbic: UIImageView!
    @IBOutlet weak var cheackEnglish: UIImageView!
    
 
    @objc func actionPickerCancel(){
        
        print("Cancel")
    }
    private func setAppSemantic() {
          // app direction
          if AppLanguage.currentLanguage().contains("en") {
              UIView.appearance().semanticContentAttribute = .forceLeftToRight
              UILabel.appearance().semanticContentAttribute = .forceLeftToRight
              UITextField.appearance().textAlignment = .left
              UITextView.appearance().textAlignment = .left
              UITableView.appearance().semanticContentAttribute = .forceLeftToRight
              UITabBar.appearance().semanticContentAttribute = .forceLeftToRight
          } else if AppLanguage.currentLanguage().contains("ar") {
              UIView.appearance().semanticContentAttribute = .forceRightToLeft
              UILabel.appearance().semanticContentAttribute = .forceRightToLeft
              UITextField.appearance().textAlignment = .right
              UITextView.appearance().textAlignment = .right
              UITableView.appearance().semanticContentAttribute = .forceRightToLeft
              UITabBar.appearance().semanticContentAttribute = .forceRightToLeft
          }
      }
    
    override func viewWillAppear(_ animated: Bool) {
        if(AppLanguage.currentLanguage().contains("ar")){
            self.arbicView.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 0.1200196143)
            self.englishView.backgroundColor = UIColor.white
            self.cheackArbic.isHidden = false
            self.cheackEnglish.isHidden = true
        }else{
            self.arbicView.backgroundColor = UIColor.white
            self.englishView.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 0.1200196143)
            self.cheackArbic.isHidden = true
            self.cheackEnglish.isHidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
       
        self.navigationController?.navigationBar.topItem?.title = ""
        let Arbicgesture = UITapGestureRecognizer(target: self, action: #selector(changeToArbic))
          
            self.arbicView.addGestureRecognizer(Arbicgesture)
        
        let Englishgesture = UITapGestureRecognizer(target: self, action: #selector(changeToEnglish))
          
            self.englishView.addGestureRecognizer(Englishgesture)
        
        
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
      
    }
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func changeToEnglish(){
       
        if AppLanguage.currentLanguage().contains("ar"){
            self.arbicView.backgroundColor = UIColor.white
            self.englishView.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 0.1200196143)
            
            AppLanguage.setAppLanguage(lang: "en")
            Localizer.DoTheExhange()
            self.setAppSemantic()
           
            self.BackHome()
        }
    }
    
    @objc func changeToArbic(){
        if AppLanguage.currentLanguage().contains("en"){
            self.arbicView.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 0.1200196143)
            self.englishView.backgroundColor = UIColor.white
            
            AppLanguage.setAppLanguage(lang: "ar")
            Localizer.DoTheExhange()
            self.setAppSemantic()
        

            self.BackHome()
        }
    }
    
    func BackHome () {
        SKActivityIndicator.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            guard let window = UIApplication.shared.keyWindow else { return }
            let vc = Storyboard.Main.viewController(StartNavBar.self)
            window.rootViewController = vc
            SKActivityIndicator.dismiss()
        }
    }
    
//    func restartApplication () {
//        let viewController = Storyboard.Main.initialViewController()
//        let navCtrl = UINavigationController(rootViewController: viewController)
//
//        guard
//                let window = UIApplication.shared.keyWindow,
//                let rootViewController = window.rootViewController
//                else {
//            return
//        }
//
//        navCtrl.view.frame = rootViewController.view.frame
//        navCtrl.view.layoutIfNeeded()
//
//        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
//            window.rootViewController = navCtrl
//        })
//
//    }
}
