//
//  URL.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/20/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//
import Foundation
import UIKit
import SwiftyJSON


typealias json = [String:AnyObject]
typealias DownloadCompleted = (JSON) -> ()
//Test
//https://agybh.4hoste.com:4577

// Live
//https://ajeebh.4hoste.com:4519

let BASE = "https://agybh.4hoste.com"
let BASE_URL = "\(BASE)/api/"
let TermsWeb_URL = "\(BASE)/terms"
let PrivecyWeb_URL = "\(BASE)/privacy"
let AboutWeb_URL = "\(BASE)/about"


let Register_URL = "\(BASE_URL)signUp"
let Login_URL = "\(BASE_URL)signIn"
let AccountActivation_URL = "\(BASE_URL)accountActivation"
let FprgetPassword_URL = "\(BASE_URL)forgetPassword"
let ReasetPassword_URL = "\(BASE_URL)resetPassword"
let updateUserlocale = "\(BASE_URL)updateUserlocale"
let SendActivation_URL = "\(BASE_URL)sendActivation"
let MyProfile_URL = "\(BASE_URL)myProfile"
let profileComments_URL = "\(BASE_URL)profileComments"
let EditProfile_URL = "\(BASE_URL)editProfile"
let Terms_URL = "\(BASE_URL)terms"
let Privecy_URL = "\(BASE_URL)privacy"
let AboutApp_URL = "\(BASE_URL)about_app"
let CreateComp_URL = "\(BASE_URL)createContact"
let MuRestrunts_URL = "\(BASE_URL)visitedPlaces"
let ChangePassword_URL = "\(BASE_URL)changePassword"
let CreateOrder_URL = "\(BASE_URL)createOrder"
let WattingOrders_URL = "\(BASE_URL)nearWaitingOrders"
let OPenOrder_URL = "\(BASE_URL)orderDetails"
let applyID_URL = "\(BASE_URL)agreeOrder"
let applyBid_URL = "\(BASE_URL)applyBid"
let Notficatios_URL = "\(BASE_URL)get_notifications"
let NotficatiosDelete_URL = "\(BASE_URL)deleteNotification"
let activeOrders_URL = "\(BASE_URL)activeOrders"
let finishedOrders_URL = "\(BASE_URL)finishedOrders"
let useractiveOrders_URL = "\(BASE_URL)useractiveOrders"
let userFinshedOrders_URL = "\(BASE_URL)userfinishedOrders"
let ViewBid_URL = "\(BASE_URL)viewBid"
let viewAllBids_URL = "\(BASE_URL)viewBids"
let agreeBid_URL = "\(BASE_URL)agreeBid"
let conversation_URL = "\(BASE_URL)conversation"
let UploadImage_URL = "\(BASE_URL)uploadFile"
let CancelOrder_URL = "\(BASE_URL)cancelOrder"
let FinshlOrder_URL = "\(BASE_URL)finishOrder"
let WithdrawOrder_URL = "\(BASE_URL)withdrawOrder"
let IssuBill_URL = "\(BASE_URL)createInvoice"
let Rateing_URL = "\(BASE_URL)ratingUser"
let StorRating_URL = "\(BASE_URL)ratingStore"
let LogOut_URL = "\(BASE_URL)logout"
let DeviceData_URL = "\(BASE_URL)deviceData"
let UpdateDeviceData_URL = "\(BASE_URL)updateDeviceData"
let UnSeenNotication_URL = "\(BASE_URL)num_notifications"
let PlaceWatingOrders_URL = "\(BASE_URL)placeWaitingOrders"
let nearWaitingOrders_URL = "\(BASE_URL)nearWaitingOrders"
let DelgatePlace_URL = "\(BASE_URL)delegatePlace"
let ExpectedPricePlace_URL = "\(BASE_URL)expectedPrice"
let CheckUseExpectedPrice_URL = "\(BASE_URL)checkUseExpectedPrice"
let AddCoupon_URL = "\(BASE_URL)addCoupon"
let Nearstores_URL = "\(BASE_URL)nearstores"
let Home_URL = "\(BASE_URL)home"
let SearchNearstores_URL = "\(BASE_URL)searchStores"
let Socials_URL = "\(BASE_URL)socials"
let Specialstores_URL = "\(BASE_URL)specialstores"
let storeDetails_URL = "\(BASE_URL)storeDetails"
let storeComments_URL = "\(BASE_URL)storeComments"
let nearProviders_URL = "\(BASE_URL)nearProviders"
let savedPlaces_URL = "\(BASE_URL)savedPlaces"
let savePlace_URL = "\(BASE_URL)savePlace"
let likeStoreComment_URL = "\(BASE_URL)likeStoreComment"
let userTickets_URL =  "\(BASE_URL)userTickets"
let userTicket_URL =  "\(BASE_URL)userTicket"
let createTicket_URL =  "\(BASE_URL)createTicket"
let customerSatisfaction_URL =  "\(BASE_URL)customerSatisfaction"
let HomeAds_URL =  "\(BASE_URL)HomeAds"
let countries_URL =  "\(BASE_URL)countries"
let confirmPay_URL =  "\(BASE_URL)gotCash"
let mobileLogin = "\(BASE_URL)mobileLogin"
let checkUserSignInSocial_URL = "\(BASE_URL)checkUserSignInSocial"
let yourBlance_URL = "\(BASE_URL)balance"
let balancePayInvoice_Url = "\(BASE_URL)balancePayInvoice"
let shareTitle_Url = "\(BASE_URL)share_title"
let intro_URL = "\(BASE_URL)intro"
let gotTheOrder_URL = "\(BASE_URL)gotTheOrder"
let reachedLocation_URL = "\(BASE_URL)reachedLocation"
let view_ad_URL = "\(BASE_URL)view_ad"
let reachedStore_URL = "\(BASE_URL)reachedStore"
let minDelivery_URL = "\(BASE_URL)min_delivery_price"
let settings_URL = "\(BASE_URL)settings"
let replacePoints_URL = "\(BASE_URL)replacePoints"
let changePaymentMethod_URL = "\(BASE_URL)changePaymentMethod"
let useropenOrders_URL = "\(BASE_URL)useropenOrders"
let storeProducts_URL = "\(BASE_URL)storeProducts"
let DeleteAllNotification_URL = "\(BASE_URL)deleteAllNotification"
let useractiveOrdersWithoutDelivery_URL = "\(BASE_URL)useractiveOrdersWithoutDelivery"
let userfinishedOrdersWithoutDelivery_URL = "\(BASE_URL)userfinishedOrdersWithoutDelivery"
