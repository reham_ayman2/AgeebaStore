//
//  moreVC.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 07/11/2021.
//

import UIKit

class moreVC: UIViewController {
    //[ "Profile".localized , "Menus Sections".localized , "Sections to add products".localized , "Wallet".localized , "Comments and ratings".localized,  "About the app".localized , "call us".localized , "Change Language".localized , "LogOut".localized ]
    let items = [ "Profile".localized() ,
                  "Menus Sections".localized() ,
                  "Sections to add products".localized() ,
                  "Wallet".localized() ,
                  "Comments and ratings".localized() ,
                  "About the app".localized() ,
                  "call us".localized() ,
                  "Change Language".localized() ,
                  "LogOut".localized()
    ]
    
    
    let itemImages = ["noun_setting" , "noun_menu-1" , "noun_menu-1" , "noun_wallet-1" , "noun_comment" , "noun_about" , "noun_call" , "noun_setting", "noun_logout-1"  ]
    
    
    var phoneNumber = ""

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var tableVIew: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUpView()
        self.getShareText()
   
    }
    

 
   
    @IBAction func notification(_ sender: UIButton) {
        let vc = Storyboard.Main.viewController(NotificationsVC.self)
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func setUpView (){
        
        self.userImage.MakeRounded()
        self.backView.addTopLeftCoreners(num: 30)
        tableVIew.delegate = self
        tableVIew.dataSource = self
        tableVIew.tableFooterView = UIView()
        self.userName.text = User.currentUser?.name
        self.userEmail.text = User.currentUser?.email
        self.userImage.setImageWith(User.currentUser?.avatar)
        
        
    }
    
}
