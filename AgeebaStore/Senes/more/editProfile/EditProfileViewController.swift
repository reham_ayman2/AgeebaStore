
//  EditProfileViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/26/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Photos
import EzPopup
import GoogleMaps
import SKActivityIndicatorView

class EditProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate, selectCity {
   
    
    var imagesData = [Data]()
    var imagesKeys = [String]()
    var flagImage = false
    var imageStr = ""
    var imageData:Data!
    //location
    var lat = User.currentUser?.lat
    var lng = User.currentUser?.long
    var userEmail = ""
    var gender = ""
   
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var changePassword: UIButton!
    @IBOutlet weak var addImageBtn: UIButton!
    @IBOutlet weak var userImage: UIImageView!
  
    @IBOutlet weak var backButton: UIButton!

    @IBOutlet weak var userName: UITextField!
    
    @IBOutlet weak var email: UITextField!
    
    @IBAction func goBack(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addImageAction(_ sender: Any) {
        print("😃🔍hiiiiii")
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPickerController, animated: false, completion: nil)
    }

    @IBAction func ChangePassword(_ sender: Any) {
        let forgetPass = Storyboard.Main.viewController(ChangePasswordViewController.self)
        let popupVC = PopupViewController(contentController: forgetPass, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 400)
        popupVC.backgroundAlpha = 0.5
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 10
        popupVC.shadowEnabled = true
        
                        
                        
        self.present(popupVC, animated: true)
    }
    
    
    func selectCity(iso: String, key: String, isCity: Bool, name: String) {
        
      
    }
//
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
      //  self.gender = getUserGender()
        self.userName.isUserInteractionEnabled = true
        self.userName.isUserInteractionEnabled = true
        self.userName.isUserInteractionEnabled = true
       
        self.changePassword.setTitle("Change Password".localized, for: .normal)
        self.save.setTitle("Save".localized, for: .normal)
        self.userImage.setImageWith(User.currentUser?.avatar)
        self.userName.text = getUserName()
        self.email.text =  getUSerEmail()
        //self.genderText.textField.text = getUserGender()
        self.userImage.borderColor = UIColor.lightGray.withAlphaComponent(30)
        self.userImage.borderWidth = 1.5
        self.userImage.clipsToBounds = true
        self.userImage.cornerRadius = 50
      
        //self.backButton.isHidden = true

    }
    
    
   
    @objc func FoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.BasicColor.cgColor
        text.layer.borderWidth = 2
    }
    
    @objc func UnFoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.clear.cgColor
        text.layer.borderWidth = 1
    }
    @IBAction func saveImageAction(_ sender: Any) {
        SKActivityIndicator.show()
        if(flagImage == false){
            imagesData = []
            imagesKeys = []
        }else{
            imagesData.append(self.imageData!)
            imagesKeys.append("avatar")
        }
        self.EditProfile(name: self.userName.text!)
    }
    
    
    internal func imagePickerController(_ picker:UIImagePickerController,didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]){
       
        let image_data = info[.originalImage] as! UIImage
        self.imageData = (image_data).jpegData(compressionQuality: 0.45)! as Data
        self.userImage.image = image_data
        imageStr = imageData.base64EncodedString()
        userImage.backgroundColor = UIColor.clear
        self.dismiss(animated: true, completion: nil)
        self.addImageBtn.setImage( nil, for:.normal)
        flagImage = true
        
    }
    @IBAction func openLocationAction(_ sender: Any) {
     
    }
    
    @IBAction func selectGender(_ sender: Any) {
         let popupVC = Storyboard.Main.viewController(selectCityViewController.self)
        popupVC.delegate = self
        popupVC.isGender = true
     
        present(popupVC, animated: true, completion: nil)
    }
    
    func EditProfile(name:String){
        let parmas:Parameters = [
            "name":name,
            "user_id": User.currentUser?.id ?? "" ,
            "email":self.email.text!,
            "gender":self.gender
        ]
        API.POSTImage(url: EditProfile_URL, Images: imagesData, Keys: imagesKeys, header: ["Authorization":"Bearer" + getTokenId() ,"lang":getServerLang()], parameters: parmas) { (succss, value) in
            if succss{
                SKActivityIndicator.dismiss()
                print("🌸\(value)")
                
                showSuccessAlert(title: "", message: "Modified successfully".localized)
                let data = value["data"] as? [String:Any]
                let address =  data?["address"] as? String ?? ""
                let name = data?["name"]  as? String ?? ""
                let email = data?["email"]  as? String ?? ""
                let avatar = data?["avatar"] as? String ?? ""
                let googlekey = data?["googlekey"]  as? String ?? ""
                GMSServices.provideAPIKey(googlekey)
                
                
                defaults.set(googlekey, forKey: GOOGLE_KEY)
                defaults.set(avatar, forKey: User_Avatar)
                defaults.set(name, forKey: User_Name)
                defaults.set(address, forKey: User_Address)
                defaults.set(email, forKey: User_Email)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    guard let window = UIApplication.shared.keyWindow else { return }
                    let vc = Storyboard.Main.viewController(StartNavBar.self)
                    window.rootViewController = vc
                }
          
                
       
                
              
            }else{
                SKActivityIndicator.dismiss()
                showErrorAlert(title: "", message: "Something went wrong and tried again".localized)
               
            }
        }
    }
    
    
    
}
