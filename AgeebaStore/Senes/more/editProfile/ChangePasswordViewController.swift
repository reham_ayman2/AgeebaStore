//
//  ChangePasswordViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/5/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SKActivityIndicatorView
class ChangePasswordViewController: UIViewController {
    @IBOutlet weak var oldPass: BorderBaddedTextField!
    @IBOutlet weak var newPass: BorderBaddedTextField!
    @IBOutlet weak var newpassConfirma: BorderBaddedTextField!
    @IBOutlet weak var saveBtn: RoundedButton!
    
    @IBOutlet weak var oldPasswordShowIcon: UIButton!
    @IBOutlet weak var newPasswordShowIcon: UIButton!
    @IBOutlet weak var confirmPasswordShowIcon: UIButton!

    @IBAction func saveAction(_ sender: Any) {
        if(oldPass.text?.isEmpty == true || newPass.text?.isEmpty == true || newpassConfirma.text?.isEmpty == true){
           
            showWarningAlert(title: "", message: "Please complete your Data".localized)
        }else if (newPass.text! != newpassConfirma.text!){
            
            showWarningAlert(title: "", message: "Your password and password confirmation do not match".localized)
        }else{
            SKActivityIndicator.show()
            self.changePassword(current_password: self.oldPass.text!, password: self.newPass.text!)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = ""
        self.hideKeyboardWhenTappedAround()
        self.oldPass.setLeftPaddingPoints(10)
        self.oldPass.setRightPaddingPoints(40)
        self.newPass.setLeftPaddingPoints(10)
        self.newPass.setRightPaddingPoints(40)
        self.newpassConfirma.setLeftPaddingPoints(10)
        self.newpassConfirma.setRightPaddingPoints(40)
        
        oldPass.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        oldPass.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)
        newPass.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        newPass.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)
        newpassConfirma.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        newpassConfirma.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)
    }
    @objc func FoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.BasicColor.cgColor
        text.layer.borderWidth = 2
    }
    
    @objc func UnFoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.clear.cgColor
        text.layer.borderWidth = 1
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Change Password".localized
 
    }
    @IBAction func showOLdPassword(_ sender: Any) {
        if oldPass.isSecureTextEntry {
            self.oldPasswordShowIcon.setImage(#imageLiteral(resourceName: "view-1"), for: .normal)
            oldPass.isSecureTextEntry = false
        }else{
            self.oldPasswordShowIcon.setImage(#imageLiteral(resourceName: "unview"), for: .normal)
            oldPass.isSecureTextEntry = true
        }
    }
    @IBAction func showNewPassword(_ sender: Any) {
        if newPass.isSecureTextEntry {
            self.newPasswordShowIcon.setImage(#imageLiteral(resourceName: "view-1"), for: .normal)
            newPass.isSecureTextEntry = false
        }else{
            self.newPasswordShowIcon.setImage(#imageLiteral(resourceName: "unview"), for: .normal)
            newPass.isSecureTextEntry = true
        }
    }
    @IBAction func showConfirmPassword(_ sender: Any) {
        if newpassConfirma.isSecureTextEntry {
            self.confirmPasswordShowIcon.setImage(#imageLiteral(resourceName: "view-1"), for: .normal)
            newpassConfirma.isSecureTextEntry = false
        }else{
            self.confirmPasswordShowIcon.setImage(#imageLiteral(resourceName: "unview"), for: .normal)
            newpassConfirma.isSecureTextEntry = true
        }
    }
    
    func changePassword(current_password:String,password:String){
        let params:Parameters = [
            "current_password":current_password,
            "password":password
        ]
        API.POST(url: ChangePassword_URL, parameters: params, headers: ["Authorization":"Bearer" + User.currentUser!.token ,"lang":getServerLang()]) { (succss, value) in
            if succss{
                SKActivityIndicator.dismiss()
                let key = value["key"] as! String
                let msg = value["msg"] as! String
                if(key == "fail"){
                  
                   showErrorAlert(title: "", message: msg)
                }else{
                    print("💕\(value)")
                    showSuccessAlert(title: "", message: msg)
                    
                    self.dismiss(animated: true)
                }
                
                
            }else{
                SKActivityIndicator.dismiss()
                print("💕\(value)")
            }
        }
    }
    
}
