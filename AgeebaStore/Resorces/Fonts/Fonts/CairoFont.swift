//
//  CairoFont.swift
//  TaxiAwamerCaptain
//
//   Created by Sara Ashraf on 11/18/19.
//  Copyright © 2019 aait. All rights reserved.
//

import Foundation
enum CairoFont: String, AppFont {
    case light = "Cairo-Light"
    case regular = "Cairo-Regular"
    case semiBold = "Cairo-SemiBold"
    case bold = "Cairo-Bold"
}
