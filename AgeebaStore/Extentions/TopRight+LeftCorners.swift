//
//  TopRight+LeftCorners.swift
//  Ra3ia
//
//  Created by Sara Mady on 24/03/2021.
//

import Foundation
import UIKit
extension UIButton {
    
    
    func AddTOPCorners (num : CGFloat ) {
            
      layer.cornerRadius = num
      layer.maskedCorners = [.layerMaxXMinYCorner , .layerMinXMinYCorner]
         
    }
}
