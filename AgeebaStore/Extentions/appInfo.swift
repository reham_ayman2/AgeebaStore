//
//  appInfo.swift
//  AgeebaStore
//
//  Created by Reham Ayman on 20/11/2021.
//

import Foundation
import Localize_Swift
func SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_ v: String) -> Bool {
    return UIDevice.current.systemVersion.compare(v, options: .numeric) != .orderedAscending
}

func getServerLang () -> String{
    if (AppLanguage.currentLanguage().contains(Lang_ar)){
        return "ar"
    }
    
    return "en"
}
func getScreenWidth() -> CGFloat {
    let screenSize = UIScreen.main.bounds
    let screenWidth = screenSize.width
    return screenWidth
}

func getScreenHeight() -> CGFloat {
    let screenSize = UIScreen.main.bounds
    let screenHeight = screenSize.width
    return screenHeight
}
