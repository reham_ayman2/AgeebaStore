//
//  RoundedImage.swift
//  Ra3ia
//
//  Created by Sara Mady on 24/03/2021.
//

import Foundation
import UIKit


extension UIImageView {

    func MakeRounded() {

       
        self.layer.masksToBounds = false
        self.layer.borderColor = #colorLiteral(red: 0.9646111131, green: 0.964772284, blue: 0.9645897746, alpha: 1)
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
  
    
}
