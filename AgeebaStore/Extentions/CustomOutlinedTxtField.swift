//
//  CustomOutlinedTxtField.swift
//  Pingek
//
//  Created by Sara Ashraf on 4/18/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//


import Foundation
import MaterialComponents.MaterialTextFields
import MaterialComponents.MaterialTextFields_ColorThemer

import UIKit


class CustomOutlinedTxtField:UIView{

 var textFieldControllerFloating: MDCTextInputControllerOutlined!
var textField: MDCTextField!
    
    @IBInspectable var placeHolder: String!
    @IBInspectable var value: String!
    @IBInspectable var primaryColor: UIColor! = UIColor.BasicColor
    
    @IBInspectable var placeHolderLocalized: String? {
        set {
            placeHolder = newValue?.localized
        }get {
            return placeHolder
        }
    }
    
override open func draw(_ rect: CGRect) {
    super.draw(rect)

    textField.frame = CGRect(x: 0, y: 0, width: self.frame.size.width , height: self.frame.height)

}
open override func awakeFromNib() {
    super.awakeFromNib()
   
    setUpProperty()
}


    
func setUpProperty() {
    //Change this properties to change the propperties of text
    textField = MDCTextField(frame: CGRect(x: 0, y: 0, width: self.frame.size.width , height:self.frame.size.height))
    textField.placeholder = placeHolder
    textField.text = value
    
  // textField.backgroundColor = UIColor.red
   
   // textField.setRightPaddingPoints(20)
   // textField.font =  UIFont(name: CairoFont.regular.rawValue, size: 14)
    textField.textColor = UIColor.darkGray
    //Change this properties to change the colors of border around text
    textField.placeHolderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)
    
    textFieldControllerFloating = MDCTextInputControllerOutlined(textInput: textField)
    textFieldControllerFloating.activeColor = primaryColor
    textFieldControllerFloating.leadingViewTrailingPaddingConstant()
    textFieldControllerFloating.floatingPlaceholderActiveColor = primaryColor
    textFieldControllerFloating.isFloatingEnabled = true
    textFieldControllerFloating.normalColor = UIColor.lightGray
 
    textFieldControllerFloating.inlinePlaceholderColor = UIColor.lightGray
  
    textFieldControllerFloating.floatingPlaceholderNormalColor = primaryColor
    textFieldControllerFloating.errorColor = UIColor.red
    textFieldControllerFloating.inlinePlaceholderFont = UIFont(name: CairoFont.regular.rawValue, size: 14)
    textFieldControllerFloating.textInputFont = UIFont(name: CairoFont.regular.rawValue, size: 14)

    textFieldControllerFloating.textInsets(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
   
   // self.backgroundColor = UIColor.blue
    self.addSubview(textField)
    //self.bringSubviewToFront(self.textField)
}
    
    
}
