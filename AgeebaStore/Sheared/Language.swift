//
//  Language.swift
//  SeaTrips
//
//  Created by Sara Mady on 10/27/20.
//  Copyright © 2020 Sara Ashraf. All rights reserved.
//

import Foundation

class Language {
    class func currentLanguage() -> String {
        let def = UserDefaults.standard
        if let lang = def.object(forKey: "lang") as? String{
            return lang
        }else{
            return ""
        }

    }
    
    class func setAppLanguage(lang: String) {
        let def = UserDefaults.standard
        def.set(lang, forKey: "lang")
        def.synchronize()
    }
    
    class func getLang() -> String{
        if(currentLanguage().contains("en")){
            return "en"
        }else if(currentLanguage().contains("ar")){
            return "ar"
        }else{
            return "ur"
        }
    }
    
}

class AppLanguage {
    class func currentLanguage() -> String {
        let def = UserDefaults.standard
        let langs = def.object(forKey: "AppleLanguages") as! NSArray
        let firstLang = langs.firstObject as! String
        return firstLang
    }
    
    class func getLang() -> String{
        print("🎠\(currentLanguage())")
        if(currentLanguage().contains("en")){
            return "en"
        }else if(currentLanguage().contains("ar")){
            return "ar"
        }else{
            return "ur"
        }
    }
    
    
   class func setAppLanguage(lang: String) {
        let def = UserDefaults.standard
        let langs = def.object(forKey: "AppleLanguages") as! NSArray
        var selectedLang = lang
        for l in langs {
            if (l as! String).contains(lang){
                selectedLang = l as! String
            }
        }
    
        def.set([selectedLang], forKey: "AppleLanguages")
        def.synchronize()
    }
}
